#!/usr/bin/php
<?php

$dir    = dirname(__DIR__);
$appDir = $dir . '/app';

// Necessary requires to get things going
require $appDir . '/library/interfaces/IRun.php';
require $appDir . '/library/application/Cron.php';


// Necessary paths to autoload & config settings
$configPath         = $appDir . '/config/';
$config             = $configPath . 'config.php';
$autoLoad           = $configPath . 'autoload.php';


use \Models\Api as Api;

try {
	$app = new Application\Cron();

	/**
	 * If the application throws an HTTPException, send it on to the client as json.
	 * Elsewise, just log it.
	 * TODO:  Improve this.
	 */
	function exceptionHandler($exception){
		error_log($exception);
		error_log($exception->getTraceAsString());
	}
	set_exception_handler('exceptionHandler');

	// Setup App (dependency injector, configuration variables and autoloading resources/classes)
	$app->appDir = $appDir;
	$app->setAutoload($autoLoad, $appDir);
	$app->setConfig($config);

    /*
        ICD10 Table Version Updater
     */
    $icd10Cron = new \Cron\Job\Icd10Cron();
    $icd10Cron->doCron();


} catch(\UnexpectedValueException $e) {
	// This is error is for catching JWT Firebase ErrorException
	throw new \Micro\Exceptions\HTTPExceptions(
		$e->getMessage(),
		401,
		array(
			'dev' => '',
			'internalCode' => 'TK0000',
			'more' => ''
		)
	);
} catch(Exception $e) {

	throw new \Micro\Exceptions\HTTPExceptions(
		$e->getMessage(),
		500,
		array(
			'dev' => 'Something went wrong on Processing Request',
			'internalCode' => 'NF0000',
			'more' => ''
		)
	);
}


?>
