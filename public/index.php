<?php

/**
 * Driver for PHP HMAC Restful API using PhalconPHP's Micro framework
 *
 * @package None
 * @author  Jete O'Keeffe
 * @license none
 */

// echo phpinfo();
// die();
//die("somethings fishy");
// Setup configuration files
$dir = dirname(__DIR__);
$appDir = $dir . '/app';

// Necessary requires to get things going
require $appDir . '/library/utilities/debug/PhpError.php';
require $appDir . '/library/interfaces/IRun.php';
require $appDir . '/routes/RouteLoader.php';
require $appDir . '/library/application/Micro.php';

//Need Amazon S3 to Load
// require $appDir . '/library/utilities/Amazon/aws-autoloader.php';

// require $dir . '/vendor/autoload.php';

// Capture runtime errors
//register_shutdown_function(['Utilities\Debug\PhpError','runtimeShutdown']);

// Necessary paths to autoload & config settings
$configPath = $appDir . '/config/';
$config = $configPath . 'config.php';
$autoLoad = $configPath . 'autoload.php';
$formAutoload = $appDir . '/form/autoload/form-autoload.php'; // Dynamic Form namespace
$amazonautoLoad = $appDir . '/library/utilities/Amazon/aws-autoloader.php';
$routes = $configPath . 'routes.php';

// OASIS Constant Values
$oasisConstant =  $appDir. '/constants/oasis/conOasis.php';

use \Models\Api as Api;

try {
	$app = new Application\Micro();

	/**
	 * If the application throws an HTTPException, send it on to the client as json.
	 * Elsewise, just log it.
	 * TODO:  Improve this.
	 */
	function exceptionHandler($exception){
		error_log($exception);
		error_log($exception->getTraceAsString());
	}
	set_exception_handler('exceptionHandler');
	// Record any php warnings/errors
	//set_error_handler(['Utilities\Debug\PhpError','errorHandler']);

	// Setup App (dependency injector, configuration variables and autoloading resources/classes)
	$app->appDir = $appDir;
	$app->setAutoload($autoLoad, $appDir, $formAutoload);
	$app->setConfig($config, $oasisConstant);

	//Attach Events to Micro
	$events = [
		new \Events\Api\TokenAuthentication($app->request->getHeader('Authorization'),$app->config->hashkey),
		new \Events\Acl(),
		new \Events\Firewall($app->request->getHeader('Authorization'))
	];
	$app->setEvents($events);

	// Setup RESTful Routes
	$app->setRoutes($routes);

	// Boom, Run
	$app->run();


} catch(\UnexpectedValueException $e) {
	// This is error is for catching JWT Firebase ErrorException
	throw new \Micro\Exceptions\HTTPExceptions(
		$e->getMessage(),
		401,
		array(
			'dev' => '',
			'internalCode' => 'TK0000',
			'more' => ''
		)
	);
} catch(Exception $e) {

	throw new \Micro\Exceptions\HTTPExceptions(
		$e->getMessage(),
		500,
		array(
			'dev' => 'Something went wrong on Processing Request',
			'internalCode' => 'NF0000',
			'more' => ''
		)
	);
}
