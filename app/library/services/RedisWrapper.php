<?php

/**
 * Event that check JWT Authentication
 *
 * @package Events
 * @subpackage Api
 * @author Jete O'Keeffe
 * @version 1.0
 */

namespace Services;

use Constants\NavConstants;

class RedisWrapper extends \Phalcon\DI\Injectable{

    protected $_redis;

    public function __construct(){

        $app = $this->getDi()->get('application');
        // $app = $this->get('application');

        $redis = new \Redis();
        $redis->connect($app->config->redis->host, $app->config->redis->port);

        // $redis->setOption(\Redis::OPT_SERIALIZER, \Redis::SERIALIZER_NONE);   // don't serialize data
        // $redis->setOption(\Redis::OPT_SERIALIZER, \Redis::SERIALIZER_PHP);    // use built-in serialize/unserialize
        // $redis->setOption(\Redis::OPT_SERIALIZER, \Redis::SERIALIZER_IGBINARY);   // use igBinary serialize/unserialize

        // $redis->setOption(\Redis::OPT_PREFIX, $app->config->redis->sessionkey); // use custom prefix on all keys
        // Will set the key, if it doesn't exist, with a ttl of 10 seconds
        // $redis->set('key', 'value', Array('nx', 'ex'=>60));
        // $now = time();
        $this->_redis = $redis;
        // $redis->expireAt('key', $now + 60);
        // var_dump($redis->ttl('key'));

    }

    public function checkRedis(){
        return $this->_redis->ping();
    }

    public function redisSet($key, $value){
        return $this->_redis->set($key, $value);
    }

    public function redisSAdd($key, $value){
        return $this->_redis->sadd($key, $value);
    }

    public function redisSMembers($key){
        return $this->_redis->smembers($key);
    }

    public function redisSIsMembers($key, $value){
        return $this->_redis->sismember($key, $value);
    }

    public function redisGet($key){
        return $this->_redis->get($key);
    }

    public function redisSRem($key, $value){
        return $this->_redis->srem($key, $value);
    }



}
