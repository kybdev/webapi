<?php

/**
 * Event that check JWT Authentication
 *
 * @package Events
 * @subpackage Api
 * @author Jete O'Keeffe
 * @version 1.0
 */

namespace Services;

use Models\Loginattempts;
use Models\UserBlacklists;
use Constants\NavConstants;

class User extends \Phalcon\DI\Injectable{

    protected $user = [];

    protected $isOwner = false;

    protected $table = null;


    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;

        //Checking if Owner
        $owner = \Models\Members::findFirst([
            "memberid = ?0 AND employeetype='Owner'",
            "bind" => [
                    $user->id
                ]
            ]);
        if($owner){
            $this->isOwner = true;
        }

    }

    /**
     * @return bool
     *
     * Check if a user is currently logged in
     */
    public function loggedIn()
    {
        return !!$this->user;
    }

    /**
     * @return bool
     *
     * Check if a user is currently logged in
     */
    public function getRoles()
    {
        $rec = \Models\Memberroles::memberListRoles($this->user->id);
        return $rec;
    }

    public function isLoggedInUser($id){
        return $this->user->id == $id;
    }

    public function isRolePermitted($role)
    {

        if($this->isOwner){
            return true;
        }

        $rec = \Models\Memberroles::findFirst([
                "memberid = ?0 AND role =?1",
                "bind" => [
                    $this->user->id,
                    $role
                ]
            ]);

        if($rec){
            return true;
        }

        return false;
    }

    public function getUserComplete()
    {
        $user = \Models\Members::findFirst([
            'memberid=?0',
            'bind' => [$this->user->id]
            ]);

        $roles = \Models\Memberroles::memberListRoles($this->user->id);

        $rec = $user->toArray();
        $rec['roles'] = $roles;

        return $rec;
    }

    protected function clearLoginAttempt($id){
        $user = Loginattempts::find([
            'userid=?0',
            'bind' => [$id]
        ]);
    }

    protected function addLoginAttempt($id){
        $user = new Loginattempts();
        $user->userid = $id;

        if(!$user->save()){
            throw new \Micro\Exceptions\HTTPExceptions(
                'Login attempt cannot be added.',
                406,
                array(
                    'dev' => 'No User Found',
                    'internalCode' => 'MC0002',
                    'more' => array("Error1", "Erro2", "Erro3")
                )
            );
        }

        return $this->checkAttempts($id);
    }

    public function checkAttempts($id){

        $user = Loginattempts::find([
            'userid=?0',
            'bind' => [$id]
        ]);

        if($user->count() >= 5){ // change to attempt set in config

            Loginattempts::deleteAttempts($id);

            $this->wap->addToUserBlacklist([
                "id" => $id,
                "reason" => "Attempting to login."
            ]);
        }

        return $user->count();

    }

    public function authorizeUser($cred, $table, $refresh = false)
    {

        $model = 'Models\\'.$table;
        $this->table = $model;

        $query  = '';

        if($refresh){
            reset($cred);
            $this->wap->firewallCheck($cred[key($cred)], 'memberid = ?');
            $query  = ' '.key($cred).'="'.$cred[key($cred)].'"';
        }else{
            $this->wap->firewallCheck($cred['username'], 'username = ?');
            $query  = ' (username="'.$cred['username'].'" OR email="'.$cred['username'].'")';
        }

        //Login Checking Firewall



        $query .= ' AND (employeestatus="ACTIVE" OR employeestatus="PENDING")';

        $user = $model::findFirst($query);

            if($user){
                if(!$refresh && !$this->security->checkHash($cred['password'], $user->password)){
                    $this->errorThrow("You might entered a wrong username or password.",["attempts"=>$this->addLoginAttempt($user->memberid)]);
                }

                $data = $this->genToken($user->getPayLoad());
                $data["refresh"] = $refresh;
                Loginattempts::deleteAttempts($user->memberid, 'all');


                //Remove redis key value
                $this->redisWrapper->redisSRem("hc_require_relogin", $user->memberid);

                return $data;
            }

            $this->errorThrow("Account does not exist.");

    }

    private function errorThrow($msg, $more = "Empty"){
        throw new \Micro\Exceptions\HTTPExceptions(
                    $msg,
                    406,
                    array(
                        'dev' => 'No User Found',
                        'internalCode' => 'MC0002',
                        'more' => $more
                    )
                );
    }

    public function genToken($payload){

        $timestamp = time();
        $payload['level'] = $this->config['clients']['user'];
        $payload['iat'] = $timestamp;
        $payload['exp'] = strtotime('+' . $this->config['tokenEXP']['token'], $timestamp);

        $accesstoken = \Firebase\JWT\JWT::encode($payload, $this->config['hashkey']);
        $rtoken = \Firebase\JWT\JWT::encode(array(
            "id" => $payload["id"],
            "exp" => strtotime('+' . $this->config['tokenEXP']['refreshToken'], $timestamp),
            "iat" => $timestamp
        ),$this->config['hashkey']);

        return array(
            'token' => $accesstoken,
            'refreshtoken' => $rtoken
        );
    }

}
