<?php

/**
 * Event that check JWT Authentication
 *
 * @package Events
 * @subpackage Api
 * @author Jete O'Keeffe
 * @version 1.0
 */

namespace Security;

use Services\UserAgent;

class Firewall extends \Phalcon\DI\Injectable{

    public function addToUserBlacklist($data){

        $ubsearch = \Models\UserBlacklists::findFirst([
                "userid = ?0", 
                "bind" =>[$data['id']]
            ]);

        $ub = $ubsearch ? $ubsearch : new \Models\UserBlacklists();
        
        $time = time();
        $ub->userid = $data['id'];
        $ub->reason = $data['reason'];
        $ub->expiration = date('Y-m-d H:i:s',strtotime('+20 minutes', $time));
        $ub->ip_address = $this->request->getClientAddress();
        $ub->operating_system = $this->userAgent->getOS();
        $ub->browser = $this->userAgent->getBrowser();

        if(!$ub->save()){
            $this->ErrorMessage->thrower(
                    406,
                    "Cannot add to blacklist.",
                    "User Blacklisting Error.",
                    "MEM002"
                );
        }
        return true;
    }

    public function firewallCheck(){
        
    }

    // public function addToBlacklist($data){

    //     $ubsearch = \Models\UserBlacklists::findFirst([
    //             "userid = ?0", 
    //             "bind" =>[$data['id']]
    //         ]);

    //     $ub = $ubsearch ? $ubsearch : new \Models\UserBlacklists();
        
    //     $time = time();
    //     $ub->userid = $data['id'];
    //     $ub->reason = $data['reason'];
    //     $ub->expiration = date('Y-m-d H:i:s',strtotime('+20 minutes', $time));
    //     $ub->ip_address = $this->request->getClientAddress();
    //     $ub->operating_system = $this->userAgent->getOS();
    //     $ub->browser = $this->userAgent->getBrowser();

    //     if(!$ub->save()){
    //         $this->ErrorMessage->thrower(
    //                 406,
    //                 "Cannot add to blacklist.",
    //                 "User Blacklisting Error.",
    //                 "MEM002"
    //             );
    //     }
    // }

}