<?php
namespace Micro\Responses;


class NonjsonResponse extends \Micro\Responses\Response{

    public function __construct(){
        parent::__construct();
    }
    public function send($records){

        $response = $this->di->get('response');

        $response->setContentType($records['Content-Type']);
        $response->setContent($records['file']);
        $response->send();
        return $this;
    }
    public function convertSnakeCase($snake){
        $this->snake = (bool) $snake;
        return $this;
    }
    public function useEnvelope($envelope){
        $this->envelope = (bool) $envelope;
        return $this;
    }
}