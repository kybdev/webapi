<?php
namespace XML;

use Constants\oasisMos;

use Models\Oasis as Oasis;

class oasisXML{

    public static function export($oasisId, $conf){

        $xmlArray = [];

        foreach (oasisMos::sectionMos as $section => $sectionMos) {

            $oasis = Oasis::clinicalRecord($oasisId, $conf, array($section));

            // var_dump($oasis);

            foreach ($sectionMos as $mo => $moInfo) {

                switch ($moInfo['formCode']) {
                    case oasisMos::formCode['01']:
                        $xmlArray = oasisXML::exportTxt($moInfo, $oasis, $xmlArray);
                    break;

                    case oasisMos::formCode['02']:
                        $xmlArray = oasisXML::exportSclt($moInfo, $oasis, $xmlArray);
                    break;

                    case oasisMos::formCode['03']:
                        $xmlArray  = oasisXML::exportTxtChkbox($moInfo, $oasis, $xmlArray);
                    break;

                    case oasisMos::formCode['04']:
                        $xmlArray  = oasisXML::exportGrouptTxt($moInfo, $oasis, $xmlArray);
                    break;

                    case oasisMos::formCode['05']:
                        $xmlArray  = oasisXML::exportDate($moInfo, $oasis, $xmlArray);
                    break;
                    //
                    // case oasisMos::formCode['06']:
                    //     echo "Your favorite color is 06! <br>";
                    // break;
                    //
                    // case oasisMos::formCode['07']:
                    //     echo "Your favorite color is 07! <br>";
                    // break;
                    //
                    // case oasisMos::formCode['08']:
                    //     echo "Your favorite color is 08! <br>";
                    // break;
                    //
                    // case oasisMos::formCode['09']:
                    //     echo "Your favorite color is 09! <br>";
                    // break;
                    //
                    // case oasisMos::formCode['10']:
                    //     echo "Your favorite color is 10! <br>";
                    // break;
                    //
                    // case oasisMos::formCode['11']:
                    //     echo "Your favorite color is 11! <br>";
                    // break;
                    //
                    // case oasisMos::formCode['12']:
                    //     echo "Your favorite color is 12! <br>";
                    // break;

                    default:
                    // echo "Your favorite color is neither red, blue, nor green!";
                }
            }

            var_dump($xmlArray);

            die();
        }
    }

    public static function exportTxt($moInfo, $oasis, $xmlArray){
        $key    = $moInfo['xmlKey'];
        $model  = $moInfo['model'];
        $value  = $oasis[$model];

        if(array_key_exists($model, $oasis)){
            $value          = $oasis[$model];
            $xmlArray[$key] = $value;

        }else{
            $xmlArray[$key] = "Model_not_Exist";
        }

        return $xmlArray;
    }

    public static function exportSclt($moInfo, $oasis, $xmlArray){

        $key    = $moInfo['xmlKey'];
        $model  = $moInfo['model'];
        $value  = $oasis[$model];

        if(array_key_exists($model, $oasis)){
            $value          = $oasis[$model];
            $xmlArray[$key] = $value;

        }else{
            $xmlArray[$key] = "Model_not_Exist";
        }
        return $xmlArray;

    }

    public static function exportTxtChkbox($moInfo, $oasis, $xmlArray){
        foreach ($moInfo['txtchk'] as $key => $value){
            $key    = $value['xmlKey'];
            $model  = $value['model'];
            if(array_key_exists($model, $oasis)){
                $value          = $oasis[$model];
                $xmlArray[$key] = $value;

            }else{
                $xmlArray[$key] = "Model_not_Exist";
            }
        }
        return $xmlArray;
    }

    public static function exportGrouptTxt($moInfo, $oasis, $xmlArray){
        foreach ($moInfo['txtgrp'] as $key => $value){
            $key    = $value['xmlKey'];
            $model  = $value['model'];
            if(array_key_exists($model, $oasis)){
                $value          = $oasis[$model];
                $xmlArray[$key] = $value;

            }else{
                $xmlArray[$key] = "Model_not_Exist";
            }
        }
        return $xmlArray;
    }

    public static function exportDate($moInfo, $oasis, $xmlArray){
        $key    = $moInfo['xmlKey'];
        $model  = $moInfo['model'];
        $value  = $oasis[$model];

        if(array_key_exists($model, $oasis)){
            $value          = $oasis[$model];

            if(strtotime($value)){
                $xmlArray[$key] = $value;
            }
        }else{
            $xmlArray[$key] = "Model_not_Exist";
        }

        return $xmlArray;
    }


}
