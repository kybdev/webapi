<?php
/**
 *  Author: Kyben C. de Guia <kc.deguia@gmail.com>
 *  Title: ICD10 Cron Job Function
 */
namespace Cron\Job;

use Phalcon\Filter;
use Template\Model\TemplateModel;

 /*
    BENCHMARK:
    Custom Insert Query = > 4000 ms (greater than 4 seconds)
 */

class Icd10Cron{

    public static function test(){
        echo "you hit Test";
    }


    public function cons(){
        return array(
            //Project Direcory
            'projectDir'    => str_replace('app/library/cronjob','', dirname(__FILE__)),

            //Generated Model file directory
            'modelDir'      =>  str_replace('library/cronjob','', dirname(__FILE__))."models/",

            //ICD File Directory
            'icdFilesDir'   => 'dist/icd/',

            //IF Value is true total rows of the file will be divided by chunk, size of chankMax
            'useChank'      => true,

            //Custom Query Maximum Allowed Data to be inserted
            'chankMax'      => 40000, // NOTE: Mysql default setting only allowed not more than 40000 records to be inserted;

            //Tables Version Text File Director
            'tlbVerFileDir' => str_replace('library/cronjob','', dirname(__FILE__))."constants/TableModelVersions.txt",

            // Table Version Belongs to
            'module'        => "icd10",

            //ICD10 Table Structure
            'tableStructure'     => array(
                "pk" => array(
                    "type"   => "CHAR",
                    "length" => 7,
                    "isNull" => true,
                    "pk"     => true // Primary Key
                ),
                "code" => array(
                    "type"   => "CHAR",
                    "length" => 7,
                    "isNull" => true,
                ),
                "shortDesc" => array(
                    "type"   => "TEXT",
                    "isNull" => false
                ),
                "longDesc" => array(
                    "type"   => "TEXT",
                    "isNull" => false
                )
            ),
        );
    }

    public static function doCron(){
        $dis = (new Icd10Cron())->cons();

        // ICD's File Directory
        $icdFileDirectory = $dis['projectDir'].$dis['icdFilesDir'];

        // Directory File Content
        $filesList        = Icd10Cron::dirFiles($icdFileDirectory);

        // Get Array list Last Index
        $lastIndexValue  = end($filesList);

        // Full File Directory
        $fileDirectory    = $icdFileDirectory.$lastIndexValue;

        // File Name
        $fileName = pathinfo($fileDirectory, PATHINFO_FILENAME);

        // File Extension
        $fileExtension = pathinfo($fileDirectory, PATHINFO_EXTENSION);

        // Create Table
        $isTableCreated = Icd10Cron::createdTable($fileName, $dis['tableStructure']);
        // $isTableCreated = true;
        // Get Create Table Return Type
        $isTableReturnType = gettype($isTableCreated);

        if(($isTableReturnType == 'boolean')){
            if($isTableCreated){
                //NOTE use a create function  depending of what the file extension  is.
                // Validate Extension
                if($fileExtension == "txt"){
                    Icd10Cron::Icd10TextFormat($fileDirectory, $fileName, $dis);
                }elseif ($fileExtension == "csv") {
                    // Use CSV function
                }
            }
        }else{
            echo "throw Error Here!";
            var_dump($isTableCreated);
        }

    }

    public static function createdTable($tableName, $tableCol){
        $db  = \Phalcon\DI::getDefault()->get('db');

        $error = [];

        // Check database if table already existed
        $stmt = $db->prepare("SHOW TABLES LIKE '".$tableName."'");
        $stmt->execute();
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);

        // If table is not yet Existed, generate new version of table in the database
        if(!$result){

            $queryStart = "CREATE TABLE `".$tableName."` (";
            $queryCol   = "";
            $queryEnd   = ") ENGINE = InnoDB";

            $ifPrimeKey = '';

            $exicute  = true;
            $incIndex = 0;
            foreach ($tableCol as $key => $value) {
                $incIndex++;

                $comma = $incIndex < count($tableCol) ? ',':'';

                //NOTE : OTHER VALIDATION FOR EACH COLUMN WILL BE ADDED DURING THIS FUNCTIONS DEVELOPMENT

                //If Type is Character check length
                if($value['type'] == "CHAR"){
                    if(! array_key_exists("length",$value)){
                        $error[] = array($key." length is required","SQLCS0001","Length of the type is Needed");
                        $exicute = false;
                        break;
                    }else{
                        $value['type'] = "CHAR(".$value['length'].")";
                    }
                }

                if(array_key_exists("isNull", $value)){
                    $isNull = $value['isNull']  ? "NOT NULL" : "NULL";
                }else{
                    $isNull = "NULL";
                }

                if(array_key_exists("pk", $value)){
                    $ifPrimeKey = $value['pk']  ? ", PRIMARY KEY (`".$key."`)" : '';
                }
                $queryCol.= "`".$key."` ".$value['type']." ".$isNull.$comma." ";
            }

            if($exicute){
                $query = $queryStart.$queryCol.$ifPrimeKey.$queryEnd;
                // echo $query;
                $exec  = $db->prepare($query);
                if($exec->execute()){

                    // Generate Table Model File;
                    $isModelCreated = Icd10Cron::generateTableModelFile($tableName);
                    $resultType = gettype($isModelCreated);

                    // If Generating of table model file failed drop the table
                    if($resultType == 'boolean'){
                        return true;
                    }else{
                        $exec = $db->prepare("DROP TABLE ".$tableName."");
                        if($exec->execute()){
                            return  true;
                        }else{
                            return $error[] = array("Something went wrong while creating table".$key."","SQLCS0002",$exec->execute()->getMessages());
                        }
                    }

                }else{
                    return $error[] = array("Something is wrong while creating table".$key."","SQLCS0002",$exec->execute()->getMessages());
                }
            }
        }else{
            return $error[] = array("Table Already Exist!".$tableName."","SQLCS0000","Table already exist!");
        }

    }

    public static function generateTableModelFile($tableName){
        $dis = (new Icd10Cron())->cons();
        $error = [];
        // GENERATE MODEL FILE  ACCORDING TO THE CREATED TABLE
        $newModel =  $dis['modelDir'].ucfirst($tableName).".php";
        $newModelContent = TemplateModel::modelDefault(ucfirst($tableName));

        $template = preg_replace('/^[ \t]*[\r\n]+/m', '', $newModelContent);

        if(file_put_contents($newModel, $template) !== false){

            //Insert table module version to TableModelVersion.txt
            $tblJson = json_decode(file_get_contents($dis['tlbVerFileDir']), true);

            // Check if module is already exist in txt object
            $module = $dis['module'];
            if(!array_key_exists($module, $tblJson)){
                $tblJson[$module];
                $inc = "01"; // Default key index
            }else{
                $incIndex = count($tblJson[$module]) + 1;
                $inc = $incIndex < 10 ? "0".$incIndex : $incIndex;
            }

            $tblJson[$module][(string) $inc] = ucfirst($tableName);

            file_put_contents($dis['tlbVerFileDir'], json_encode($tblJson));

            return  true;
        }else{
            return  array("Unable Create model file for ".$tableName,"SQLCS0003", "Unable Create file for ".$tableName);
        }
    }

    public static function Icd10TextFormat($fileDir, $fileName, $dis){
        //File Content
        $fileContent =  explode("\n", file_get_contents($fileDir));
        if($dis['useChank']){
            $chunk   = array_chunk($fileContent, $dis['chankMax']);
            Icd10Cron::chankList($chunk, $fileName,  $dis);
        }else{
            // Function for importing  CSV here
        }
    }

    public static function chankList($chunk, $fileName, $dis){
        $filter = new Filter();

        $totalChank = count($chunk);

        $insertChank = [];

        $rowIndex = "0000001";

        foreach ($chunk as $k => $chankArray){

            if(count($chankArray) > 1  ? true : false){

                $chunkTotal = 0;
                $insertData = '';
                foreach($chankArray as $key => $value){

                    $chunkTotal++;
                    if($value){
                        $comma = $chunkTotal < (count($chankArray))?',':'';

                        $icd10index     = $rowIndex;
                        $tempCode       = trim(substr($value, 0 ,7));


                        if(strlen($tempCode) > 3){
                            $icdCode =substr($tempCode,0, 3).".".substr($tempCode,3);
                        }else{
                            $icdCode = $tempCode.".";
                        }

                        $icd10Code      = $icdCode;
                        $icdShortDesc   = ltrim(substr(substr($value, 7),1));
                        $icdLongDesc    = ltrim(substr(substr($value, 7),1));

                        // $icd10index     = preg_split('/ +/', substr($value, 0,14))[0];
                        // $icd10Code      = preg_split('/ +/', substr($value, 0,14))[1];
                        // $icdShortDesc   = ltrim(substr(substr($value, 14,63),1));
                        // $icdLongDesc    = ltrim(substr( substr($value, 76), 1));

                        $insertData.="
                        (
                            '".$filter->sanitize($icd10index,   "string")."',
                            '".$filter->sanitize($icd10Code,    "string")."',
                            '".$filter->sanitize($icdShortDesc, "string")."',
                            '".$filter->sanitize($icdLongDesc,  "string")."'
                        )".$comma;



                        // // Increment ROWID
                        $str = ltrim($rowIndex) + 1;
                        $strlen = strlen($str);
                        $zero = "";
                        for ($x = 7; $x > $strlen; $x--){
                            $zero.="0";
                        }
                        $rowIndex = $zero.$str;

                    }
                }

                // Insert chunk data to the new created table
                $isInsert = Icd10Cron::insertCustomQuery($fileName , rtrim($insertData, ','));

                // Get Return Type
                $isInsertType = gettype($isInsert);

                // If Type is Not-Boolean Truncate table
                //         --other option--
                //  Delete table and the model file
                if(!($isInsertType == 'boolean')){
                    // DO SOMETHING HERE
                    break;
                }
            }
        }
    }


    public static function insertCustomQuery($fileName, $tableData){

        $db  = \Phalcon\DI::getDefault()->get('db');
        $dis = (new Icd10Cron())->cons();

        // Prepare table Column
        $incCol = 0;
        $colName = "";
        foreach ($dis['tableStructure'] as $key => $value){
            $incCol++;
            $comma = $incCol < count($dis['tableStructure'])?',':'';
            $colName.=$key.$comma;
        }

        // Custom Query Script
        $insertQuery = 'INSERT INTO '.$fileName.' ('.$colName.') VALUES'.$tableData;

        try {
            $insert = $db->prepare($insertQuery);
            if($insert->execute()){
                return true;
            }else{
                $this->ErrorMessage->thrower(406,"","Unable Set Data","SQLCS0001","Unable Set Data");
            }

        } catch (Exception $e) {
            $this->ErrorMessage->thrower(406,$e->getMessage(),"Inserting Error","SQLCS0002", "errors");
        }


    }

    // List All Files Inside a Directory
    public static function dirFiles($fileDir){
        return array_diff(scandir($fileDir), array('.', '..'));
    }
}
