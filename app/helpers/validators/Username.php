<?php
namespace RequestValidator;

use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator\PresenceOf;
use Models\Members;

class Username extends Validation
{
    public function initialize()
    {
        //Checking that must be required
        $this->add("username", new PresenceOf(["message" => "Username is required"]));
    }

}