<?php
namespace RequestValidator;

use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;
use Phalcon\Validation\Validator\PresenceOf;

class Sixtydayssummary extends Validation
{
    public function initialize()
    {
        //Checking that must be required
        $this->add("physician", new PresenceOf(["physician" => "Drug Regimen Review 1 is required",]));
        $this->add("periodFrom", new PresenceOf(["message" => "Episode Period is required",]));
        $this->add("periodTo", new PresenceOf(["message" => "Episode Period is required",]));
        // $this->add("services", new PresenceOf(["message" => "Service/s is required",]));
        // $this->add("title", new PresenceOf(["message" => "Clinician title is required",]));
        $this->add("dsDate", new PresenceOf(["message" => "Date is required",]));
        // $this->add("clinician", new PresenceOf(["message" => "Clinician is required",]));
        // $this->add("signature_memberid", new PresenceOf(["message" => "Member id signature is required",]));
    }

}