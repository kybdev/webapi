<?php
namespace RequestValidator;

use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator\PresenceOf;
use Models\Members;

class SSN extends Validation
{
    public function initialize()
    {
        //Checking that must be required
        $this->add("ssn", new PresenceOf(["message" => "SSN is required"]));
    }

}