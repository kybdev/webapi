<?php
namespace RequestValidator;

use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;
use Phalcon\Validation\Validator\PresenceOf;

class AdmissionOrder extends Validation
{
    public function initialize()
    {
        //Checking that must be required
        $this->add("orderDate", new PresenceOf(["message" => "Order date is required",]));
        $this->add("admissionid", new PresenceOf(["message" => "Admission id is required"]));
        $this->add("idpatientepisode", new PresenceOf(["message" => "Patient episode is required"]));
        $this->add("idpatient", new PresenceOf(["message" => "Patient is required"]));
        // $this->add("signature_memberid", new PresenceOf(["message" => "Signature is required"]));
    }

}