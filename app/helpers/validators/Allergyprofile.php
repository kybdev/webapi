<?php
namespace RequestValidator;

use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;
use Phalcon\Validation\Validator\PresenceOf;

class Allergyprofile extends Validation
{
    public function initialize()
    {
        //Checking that must be required
        $this->add("idpatient", new PresenceOf(["message" => "Patient id is required",]));
        $this->add("substance", new PresenceOf(["message" => "Substance is required"]));
        // $this->add("reaction", new PresenceOf(["message" => "Reaction is required"]));
        $this->add("date_occurred", new PresenceOf(["message" => "Date is required"]));
    }

}