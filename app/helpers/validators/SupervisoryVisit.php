<?php
namespace RequestValidator;

use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;
use Phalcon\Validation\Validator\PresenceOf;

class SupervisoryVisit extends Validation
{
    public function initialize()
    {
        //Checking that must be required
        $this->add("drr1", new PresenceOf(["message" => "Drug Regimen Review 1 is required",]));
        $this->add("drr2", new PresenceOf(["message" => "Drug Regimen Review 2 is required",]));
        $this->add("drr3", new PresenceOf(["message" => "Drug Regimen Review 3 is required",]));
        $this->add("drr4", new PresenceOf(["message" => "Drug Regimen Review 4 is required",]));
        $this->add("drr5", new PresenceOf(["message" => "Drug Regimen Review 5 is required",]));
        $this->add("drr6", new PresenceOf(["message" => "Drug Regimen Review 6 is required",]));
        $this->add("drr7", new PresenceOf(["message" => "Drug Regimen Review 7 is required",]));
        $this->add("drr8", new PresenceOf(["message" => "Drug Regimen Review 8 is required",]));
        $this->add("ddr_memberid", new PresenceOf(["message" => "Drug Regimen Review Done by is required",]));
        $this->add("signature_memberid", new PresenceOf(["message" => "Member id signature is required",]));
    }

}