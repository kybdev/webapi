<?php
namespace RequestValidator;

use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;
use Phalcon\Validation\Validator\PresenceOf;


class Planofcare extends Validation
{
    public function initialize()
    {
        // Checking that must be required
        $this->add("signature_memberid", new PresenceOf(["message" => "Member id signature is required",]));

    }

}
