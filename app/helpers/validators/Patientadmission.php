<?php
namespace RequestValidator;

use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;
use Phalcon\Validation\Validator\PresenceOf;

class Patientadmission extends Validation
{
    public function initialize()
    {   
        $this->add("idpatient", new PresenceOf(["message" => "Patient id is required",]));
        $this->add("socdate", new PresenceOf(["message" => "Socdate is required",]));
        $this->add("episodedate", new PresenceOf(["message" => "Episodedate is required",]));
        $this->add("insurancePrimary", new PresenceOf(["message" => "Primaryinsurance ordered is required",]));
        $this->add("refDate", new PresenceOf(["message" => "Referral date is required",]));
        $this->add("physicianAttending", new PresenceOf(["message" => "Attending physician is required",]));
        $this->add("mrnum", new PresenceOf(["message" => "MRN is required",]));
        $this->add("discipline", new PresenceOf(["message" => "discipline is required",]));

        //Checking that must be required
        // BACKUP
    	// $this->add("socdate", new PresenceOf(["message" => "Socdate is required",]));
    	// $this->add("episodedate", new PresenceOf(["message" => "Episodedate is required",]));
    	// $this->add("primaryinsurance", new PresenceOf(["message" => "Primaryinsurance ordered is required",]));
    	// $this->add("referraldate", new PresenceOf(["message" => "Referraldate is required",]));

    	// $this->add("orderdate", new PresenceOf(["message" => "Orderdate is required",]));
    	// $this->add("physician", new PresenceOf(["message" => "Physician ordered is required",]));
    	// $this->add("summary", new PresenceOf(["message" => "Summary is required",]));
    	// $this->add("finalfrequencyarray", new PresenceOf(["message" => "Frequency is required",]));
    }

}