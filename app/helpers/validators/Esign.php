<?php
namespace RequestValidator;

use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator\PresenceOf;

class Esign extends Validation
{
    public function initialize()
    {
        //Checking that must be required
        $this->add("esign", new PresenceOf(["message" => "Esign is required"]));
    }

}