<?php
namespace RequestValidator;

use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Date;
use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;
use Models\Members;

class Member extends Validation
{
    public function initialize()
    {
        //Checking that must be required
        $this->add("username", new PresenceOf(["message" => "Name is required",]));
        $this->add("email", new PresenceOf(["message" => "Email is required",]));
        $this->add("firstname", new PresenceOf(["message" => "Firstname is required",]));
        $this->add("lastname", new PresenceOf(["message" => "Lastname is required",]));
        $this->add("birthdate", new PresenceOf(["message" => "Birthdate is required",]));
        $this->add("gender", new PresenceOf(["message" => "Gender is required",]));
        $this->add("credential", new PresenceOf(["message" => "Credential is required",]));
        $this->add("title", new PresenceOf(["message" => "Title is required",]));
        $this->add("employeetype", new PresenceOf(["message" => "Employee type is required",]));
        $this->add("employeestatus", new PresenceOf(["message" => "Employee status is required",]));
        $this->add("ssn", new PresenceOf(["message" => "SSN is required",]));
        $this->add("employeeid", new PresenceOf(["message" => "Employee ID is required",]));
        $this->add("address1", new PresenceOf(["message" => "Address 1 is required",]));
        $this->add("city", new PresenceOf(["message" => "City is required",]));
        $this->add("state", new PresenceOf(["message" => "State is required",]));
        $this->add("zip", new PresenceOf(["message" => "Zip is required",]));

        //Email validity
        $this->add("email", new Email(["message" => "The e-mail is not valid"]));

        $this->add("birthdate", new Date(["format" => "Y-m-d","message" => "The birthdate is not a valid date or format"]));

        
        // $this->add('username', new UniquenessValidator([
        //         'model' => '\Models\Members',
        //         'attribute' => 'username',
        //         'message' => 'Username already taken.'
        // ]));


        // $this->add('email', new UniquenessValidator([
        //         'model' => '\Models\Members',
        //         'attribute' => 'email',
        //         'message' => 'Email already taken.'
        // ]));
    }

}
