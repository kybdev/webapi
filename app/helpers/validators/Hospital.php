<?php
namespace RequestValidator;

use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Date;
use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;


class Hospital extends Validation
{
    public function initialize()
    {
        // Checking that must be required
        $this->add("name", new PresenceOf(["message" => "Hospital Name is required",]));
        // $this->add("address", new PresenceOf(["message" => "address is required",]));
        // $this->add("zipCode", new PresenceOf(["message" => "Zip Code is required",]));
        // $this->add("phone", new PresenceOf(["message" => "Phone is required",]));
        // $this->add("fax", new PresenceOf(["message" => "Fax is required",]));
        // $this->add("contactPerson", new PresenceOf(["message" => "Contact Person is required",]));
        // $this->add("city", new PresenceOf(["message" => "City is required",]));
        // $this->add("state", new PresenceOf(["message" => "State is required",]));


    }

}
