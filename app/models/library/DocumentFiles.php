<?php

namespace LibraryModels;

use Models\Basemodel;

class DocumentFiles extends Basemodel
{
	public function initialize() {
		$this->table = 'document_files';
	}
}