<?php

namespace Models;

use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Db\Column;
use Phalcon\Mvc\Model\Relation;

class Members extends Basemodel {

    public $memberid;
    public $firstname;
    public $lastname;
    public $permission;

    public function initialize() {

    	$this->setSource('members');
    	$this->hasOne('memberid', 'Models\Logininfo', 'memberid',array('alias' => 'logininfo'));
        $this->hasMany(
                'memberid',
                'Models\Memberroles',
                'memberid',
                ['alias' =>
                    'rolesofmembers'
                ]
            );
        $this->hasMany(
            'memberid',
            'Models\Members_professional_credentials',
            'memberid',
            array(
                'foreignKey' => array(
                    'action' => Relation::ACTION_CASCADE,
                ),
            )
        );
        $this->hasMany(
            'memberid',
            'Models\Members_health_credentials',
            'memberid',
            array(
                'foreignKey' => array(
                    'action' => Relation::ACTION_CASCADE,
                ),
            )
        );
        $this->hasMany(
            'memberid',
            'Models\Members_language',
            'memberid',
            array(
                'foreignKey' => array(
                    'action' => Relation::ACTION_CASCADE,
                ),
            )
        );
    }

    public function getPayLoad(){
        $payload = array(
            "id"      => $this->memberid,
            "username"      => $this->username,
            "email"         => $this->email,
            "firstname"     => $this->firstname,
            "lastname"      => $this->lastname,
            "security"      => $this->account_security,
            "roles"         => \Models\Memberroles::memberListRoles($this->memberid, $this->employeetype == 'Owner'),
            "title"         => $this->title
        );

        return $payload;
    }

    public static function getDisplayName($memberid) {
        $member = Members::findFirst([
            'columns' => "firstname, lastname, memberid, credential",
            'memberid = :memberid:',
            'bind' => [
                'memberid' => $memberid
            ]
        ]);

        return $member ? ($member->firstname . ' ' . $member->lastname . ', ' . $member->credential) : null;
    }

    public static function listall($page=0, $count=10,$keyword=null)
    {
        $count = $count == 'all' ? 9999 : $count;
        $offset = ($page * $count) - $count;
        $end = $offset + $count;
        $options = [
            'cols'=> [
                "memberid",
                "username",
                "email",
                "firstname",
                "lastname",
                "suffix",
                "birthdate",
                "gender",
                "credential",
                "title",
                "employeetype",
                "address1",
                "address2",
                "city",
                "state",
                "zip"
            ],
            'table' => 'members',
            'after_statements'=> "ORDER BY datecreated DESC LIMIT :start , :ending ",
            'params' => ["start" => ( $offset < 0 ? 0 : $offset ), "ending" => $end],
            'types' => ["start" => Column::BIND_PARAM_INT, "ending" => Column::BIND_PARAM_INT]
        ];

        $options['conditions'] = " employeetype != 'Owner' ";

        if ($keyword!="null") {
            $options['conditions'] .= " AND
                (firstname LIKE :keyword OR
                lastname LIKE :keyword OR
                email LIKE :keyword OR
                username LIKE :keyword OR
                memberid LIKE :keyword OR
                birthdate LIKE :keyword OR
                gender LIKE :keyword OR
                credential LIKE :keyword OR
                title LIKE :keyword OR
                employeetype LIKE :keyword OR
                address1 LIKE :keyword OR
                address2 LIKE :keyword OR
                city LIKE :keyword OR
                state LIKE :keyword OR
                zip LIKE :keyword OR
                employeetype LIKE :keyword)
            ";
            $options['params']['keyword'] = '%'.$keyword.'%';
            $options['types']['keyword'] = Column::BIND_PARAM_STR;
        }

        $sql = parent::genSQL($options);

        //Counting all records
        $options['cols'] = ['COUNT(*) as count'];
        $options['after_statements'] = 'ORDER BY datecreated DESC ';
        $sqlall = parent::genSQL($options);

        // Base model
        $members = new Members();

        // Execute the query
        $queryall = $members->getReadConnection()->query($sqlall, $options['params'], $options['types']);
        $query = $members->getReadConnection()->query($sql, $options['params'], $options['types']);

        $pages = new Resultset(null, $members, $queryall);
        $data = new Resultset(null, $members, $query);

        // Get all colums pass back with its view value

        $cols = [
                "memberid" => true,
                "username" => true,
                "email" => true,
                "firstname" => true,
                "lastname" => true,
                "suffix" => false,
                "birthdate" => false,
                "gender" => false,
                "credential" => true,
                "title" => true,
                "employeetype" => true,
                "address1" => false,
                "address2" => false,
                "city" => false,
                "state" => false,
                "zip" => false,
            ];

        return [
                'data' => $data->toArray(),
                'count' => $pages->toArray()[0]['count'],
                'pages' => ceil(($pages->toArray()[0]['count'] / $count)),
                'page' => $page,
                'cols' => $cols
            ];
    }

    public static function listByDynamic($tbl_column, $value, $filter = []) {
        // Example usage of this is in AdminReportsController->getReportFilterData

        if (isset($filter["columns"])) {
            $args["columns"] = $filter["columns"];
        }

        $args["conditions"] = $tbl_column . " = ?0";
        $args["bind"] = [ $value ];
        $args["order"] = isset($filter["order"]) ? $filter["order"] : "firstname, lastname";

        return Self::find($args);
    }

}
