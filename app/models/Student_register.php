<?php

namespace Models;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Db\Column;

use Constants\RolesConstants;

class Student_register extends Basemodel {

    public function initialize() {
    }

    public static function dataList($page = 0, $count = 10, $keyword = null)
    {
        $count      = $count == 'all' ? 9999 : $count;
        $offset     = ($page * $count) - $count;
        $end        = $offset + $count;

        $cols = [
            "pkregister"    => true,
            "Name"          => true,
            "Rank"          => true,
            "AFPSN"         => true,
            "AFPID No"      => true,
            "Religion"      => false,
            "Contact"       => false,
            "Email"         => false,
        ];

        $options = [
            'cols'=> [
                "pkregister          AS pkregister",
                "CONCAT(fname, ' ', mname , ' ', lname) AS `Name`",
                "rank               AS `Rank`",
                "afpsn              AS `AFPSN`",
                "afpIdNo            AS `AFPID No`",
                "religion           AS `Religion`",
                "contact            AS `Contact`",
                "email              AS `Email`"

            ],
            'table'             => 'student_register',
            'after_statements'  => "ORDER BY datecreated DESC LIMIT :start , :ending ",
            'params'            => ["start" => ( $offset < 0 ? 0 : $offset ), "ending" => $end],
            'types'             => ["start" => Column::BIND_PARAM_INT, "ending" => Column::BIND_PARAM_INT]
        ];

        if ($keyword!="null") {
            $options['conditions'] = "

            (
                CONCAT(fname, ' ', mname , ' ', lname) LIKE :keyword OR
                rank            LIKE :keyword OR
                afpsn           LIKE :keyword OR
                afpIdNo         LIKE :keyword OR
                religion        LIKE :keyword OR
                contact         LIKE :keyword OR
                email           LIKE :keyword 
            )

            ";
            $options['params']['keyword'] = '%'.$keyword.'%';
            $options['types']['keyword'] = Column::BIND_PARAM_STR;
        }else{
            // $options['conditions'] = "dlt='1'";
        }




        $sql = parent::genSQL($options);

        //Counting all records
        $options['cols'] = ['COUNT(*) as count'];
        $options['after_statements'] = 'ORDER BY datecreated DESC ';
        $sqlall = parent::genSQL($options);

        // Base model
        $model = new Student_register();

        // Execute the query
        $queryall = $model->getReadConnection()->query($sqlall, $options['params'], $options['types']);
        $query = $model->getReadConnection()->query($sql, $options['params'], $options['types']);

        $pages = new Resultset(null, $model, $queryall);
        $data = new Resultset(null, $model, $query);





        return [
            'data' => $data->toArray(),
            'count' => $pages->toArray()[0]['count'],
            'pages' => ceil(($pages->toArray()[0]['count'] / $count)),
            'page' => $page,
            'cols' => $cols
        ];
    }


}
