<?php

namespace Models;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Db\Column;

use Constants\RolesConstants;

class Student extends Basemodel {

    public function initialize() {
    }

    public static function dataList($page = 0, $count = 10, $keyword = null)
    {
        $count      = $count == 'all' ? 9999 : $count;
        $offset     = ($page * $count) - $count;
        $end        = $offset + $count;

        $cols = [
            "studentid"     => true,
            "Name"          => true,
            "Rank"          => true,
            "AFPSN"         => true,
            "AFPID No"      => true,
            "brSvcAfpos"    => true,
            "Address"       => true,
            "City"          => false,
            "Province"      => false,
            "Region"        => false,
            "Contact No"    => false,
            "Email"         => false,
        ];

        $options = [
            'cols'=> [
                "studentid          AS studentid",
                "CONCAT(fname, ' ', mname , ' ', lname) AS `Name`",
                "rank               AS `Rank`",
                "afpsn              AS `AFPSN`",
                "afpIdNo            AS `AFPID No`",
                "brSvcAfpos         AS `brSvcAfpos`",
                "homeAddress        AS `Address`",
                "city               AS `City`",
                "province           AS `Province`",
                "region             AS `Region`",
                "contactNo          AS `Contact No`",
                "email              AS `Email`"

            ],
            'table'             => 'student',
            'after_statements'  => "ORDER BY datecreated DESC LIMIT :start , :ending ",
            'params'            => ["start" => ( $offset < 0 ? 0 : $offset ), "ending" => $end],
            'types'             => ["start" => Column::BIND_PARAM_INT, "ending" => Column::BIND_PARAM_INT]
        ];

        if ($keyword!="null") {
            $options['conditions'] = "

            (
                CONCAT(fname, ' ', mname , ' ', lname) LIKE :keyword OR
                rank            LIKE :keyword OR
                afpsn           LIKE :keyword OR
                afpIdNo         LIKE :keyword OR
                brSvcAfpos      LIKE :keyword OR
                homeAddress     LIKE :keyword OR
                city            LIKE :keyword OR
                province        LIKE :keyword OR
                region          LIKE :keyword
            )

            ";
            $options['params']['keyword'] = '%'.$keyword.'%';
            $options['types']['keyword'] = Column::BIND_PARAM_STR;
        }else{
            // $options['conditions'] = "dlt='1'";
        }




        $sql = parent::genSQL($options);

        //Counting all records
        $options['cols'] = ['COUNT(*) as count'];
        $options['after_statements'] = 'ORDER BY datecreated DESC ';
        $sqlall = parent::genSQL($options);

        // Base model
        $model = new Student();

        // Execute the query
        $queryall = $model->getReadConnection()->query($sqlall, $options['params'], $options['types']);
        $query = $model->getReadConnection()->query($sql, $options['params'], $options['types']);

        $pages = new Resultset(null, $model, $queryall);
        $data = new Resultset(null, $model, $query);





        return [
            'data' => $data->toArray(),
            'count' => $pages->toArray()[0]['count'],
            'pages' => ceil(($pages->toArray()[0]['count'] / $count)),
            'page' => $page,
            'cols' => $cols
        ];
    }


}
