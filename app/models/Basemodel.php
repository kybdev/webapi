<?php

namespace Models;

//Constants
use Constants\OrderConstants;
use Notes\Model\Notes;

class Basemodel extends \Phalcon\Mvc\Model {

    public $dateedited;
    public $datecreated;

    public function beforeValidationOnCreate()
    {
        $time = date('Y-m-d H:i:s');
        $this->datecreated = $time;
        $this->dateedited = $time;
    }

    public function beforeUpdate()
    {
        $this->dateedited = date('Y-m-d H:i:s');
    }

    public static function customQuery($phql, $params = []) {
        $di = \Phalcon\DI::getDefault();
        $dbbnb = $di->get('db');
        $stmt = $dbbnb->prepare($phql);
        if(count($params) != 0){
            foreach ($params as $param) {
                if($param['type'] == 'int'){
                    $stmt->bindParam($param['keyword'], $param['value'], \PDO::PARAM_INT);
                }
                else{
                    $stmt->bindParam($param['keyword'], $param['value'], \PDO::PARAM_STR);
                }
            }

        }
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    public static function customQueryFirst($phql, $params = []){
        $di = \Phalcon\DI::getDefault();
        $dbbnb = $di->get('db');
        $stmt = $dbbnb->prepare($phql);

        if(count($params) != 0){
            foreach ($params as $params) {
                if($params['type'] == 'int'){
                    $stmt->bindParam($params['keyword'], $params['value'], \PDO::PARAM_INT);
                }
                else{
                    $stmt->bindParam($params['keyword'], $params['value'], \PDO::PARAM_STR);
                }
            }
        }
        $stmt->execute();
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }

    public static function genSQL($options){
        return "SELECT " .
        (is_array($options['cols']) ? implode(',', $options['cols']) : $options['cols']) .
        " FROM ". $options['table'] .' ' .
        (isset($options['join']) ? $options['join'] : '') . ' ' .
        (isset($options['conditions']) ? " WHERE " . $options['conditions'] : '') . ' ' .
        (isset($options['after_statements']) ? $options['after_statements'] : '');
    }

    public static function oasisSub($a){
        $db         = \Phalcon\DI::getDefault()->get('db');
        $metaModel  = new \Phalcon\Mvc\Model\MetaData\Memory();
        $metaAtr    = $metaModel->getAttributes(new  $a['model']());
        $after = $metaAtr[2];

        // Set all the existing values to 0
        $getAll = $a['model']::find([
            "conditions"    => $a['query']['id']." = :id:",
            "bind"          => array("id" => $a['query']['value'])
        ]
        )->toArray();
        foreach ($getAll as  $value) {
            $setZero = $a['model']::findFirst([
                "conditions"    => "pk = :id:",
                "bind"          => array("id" => $value['pk'])
            ]);
            $setZero->status = "0";
            if(!$setZero->save()){
                return $setZero->getMessages();
            }
        }

        if(!empty($a['datas']) && is_array($a['datas'])) {
            // Save and Set Data Values


            if(is_array($a['datas'])){

            }else{
                echo $a['model'];
            }


            foreach ($a['datas'] as $key => $value) {
                $save = $a['model']::findFirst([
                    "conditions"    => $a['query']['id']."= :id: AND ".$metaAtr[2]." = :val:",
                    "bind"          => array("id" => $a['query']['value'], "val" => $value)

                ]);
                if($save){
                    // UPDATE DATA
                    $setSave = $a['model']::findFirst([
                        "conditions"    => "pk = :id:",
                        "bind"          => array("id" => $save->pk)
                    ]);
                    $setSave->status = "1";
                    if(!$setSave->save()){
                        return $setSave->getMessages();
                    }
                }else{
                    // INSERT NEW DATA
                    $valArray = array();
                    $valArray[$a['query']['id']]    = $a['query']['value'];
                    $valArray[$after]               = $value;
                    $valArray['status']             = "1";
                    $valArray['datecreated']        = date('Y-m-d H:i:s');
                    $valArray['dateupdated']        = date('Y-m-d H:i:s');

                    $save = new $a['model']();
                    $save->assign($valArray);
                    if(!$save->save()){
                        return $save->getMessages();
                    }
                }
            }
        }

        return true;
    }
    /**
     * Fetch Related Table data(s)
     * @param  [Array]  $parent     [Main/Parent Table]
     * @param  [String] $colName    [Sub-table data column name]
     * @param  [String] $colValue   [Foreign Key Data Value]
     * @param  [array]  $childTable [All Child table array]
     * @return [array]              [To be Fetched Data Collection]
     */
    public static function fetchChildTableData($parent, $colName, $colValue, $childTable){
        foreach ($childTable as $key => $value){
            if($value['bol']){
                $parent[$value['col']] = array_column($value['model']::find(["columns"=>"". $value['col']."", "conditions"=>"".$colName." = :id: AND status = '1'", "bind" => array("id" => $colValue)])->toArray(),  $value['col']);
            }
        }
        return $parent;
    }

    public static function get_discipline($memberid, $signature_memberid, $force = false) {
        $data = array();

        if(!empty($memberid)) {
            $signature = Members::findFirst([
                'columns' => "firstname, lastname, memberid, credential",
                'memberid = :memberid:',
                'bind' => [
                    'memberid' => $memberid
                ]
            ]);

            if($signature) {
                if($signature_memberid === $memberid || $force) {
                    $data['signature'] = $signature->firstname . $signature->lastname.",".$signature->credential;
                }
                $data['discipline'] = [
                    "memberid" => $signature->memberid,
                    "firstname" => $signature->firstname,
                    "lastname" => $signature->lastname,
                    "credential" => $signature->credential
                ];
            }
        }

        return $data;
    }

    public static function get_episode_doc_count($admissionid, $code = null){
        $data = array();

        $episode_doc_count = "";

        $order_codes = OrderConstants::CODE;

        $admission = Patientadmission::findFirst([
            "columns" => "mrnum",
            "admissionid = :admissionid:",
            "bind" => [
                "admissionid" => $admissionid
            ]
        ]);

        if(!$admission) {
            return false;
        }

        $data['mrnum'] = $admission->mrnum;

        $episodeCount = Patientepisode::count([
            "admissionid = :admissionid:",
            "bind" => [
                "admissionid" => $admissionid
            ]
        ]);

        if($code === OrderConstants::CODE[2]) { $episodeCount++; }
        $data['episode_count'] = sprintf('%02d', $episodeCount);

        $patient_form = Patient_form::findFirst([
            "columns" => "docid",
            "admissionid = :admissionid:",
            "bind" => [
                "admissionid" => $admissionid
            ],
            "order" => "docid DESC"
        ]);


        if(!$patient_form) {
            $data['form_count'] = 1;
        } else {
            $docid = $patient_form->docid;
            $data['form_count'] = intval(substr($docid, -4)) + 1;
        }

        return $data;
    }

    public static function unique_multidim_array($array, $key) {
        $temp_array = array();
        $i = 0;
        $key_array = array();
        foreach($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $temp_array[$i] = $val;
            }
            $i++;
        }
        return $temp_array;
    }

    public static function formatBytes($bytes, $precision = 2) {
        $units = array('B', 'KB', 'MB', 'GB', 'TB');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        // Uncomment one of the following alternatives
        // $bytes /= pow(1024, $pow);
        // $bytes /= (1 << (10 * $pow));

        return round($bytes, $precision) . ' ' . $units[$pow];
    }

    public static function format_date($date, $format = 'm/d/Y') {
        if($date !== "0000-00-00" && $date !== null) {
            $date = date($format, strtotime($date));
        } else {
            $date = null;
        }

        return $date;
    }

    public static function format_time($time, $format = 'H:i') {
        if($time !== "00:00:00" && $time !== null) {
            $time = date($format, strtotime($time));
        } else {
            $time = null;
        }

        return $time;
    }

    public static function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text))
        {
        return 'n-a';
        }

        return $text;
    }


    public static function formChecker($docstatus, $taskid, $dis){

        $forQa      = false; // Default
        $qaComplete = false; // Default
        $usrRole    = array();

        $taskDetails = Notes::taskDetails($taskid);

        if($dis->User->getUser()){
            if($docstatus == "on-progress" || $docstatus == "reopened" ){
                $forQa = $dis->User->isLoggedInUser($taskDetails['assignee']);
            }elseif ($docstatus == "ready-for-qa") {
                $qaComplete  =  $dis->User->isLoggedInUser($taskDetails['cm'])?:$dis->User->isLoggedInUser($taskDetails['qam']);
            }

            // CHECK iF USER HAS THE ROLE OF ADMINISTRATOR OR QA-MANAGER
            $usrRole = $dis->User->getUser()->roles->pc;

            if(in_array("pc-admin", $usrRole)){
                $forQa      = true; // Default
                $qaComplete = true; // Default
            }
        }


        return array("forQA" => $forQa, "qaComplete"=> $qaComplete, 'userRoles'=>$usrRole);
    }


    public static function curl($url) {

        $service_url = $url;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, '/windows nt 6.2/i');
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        curl_close($curl);
        return $decoded = json_decode($curl_response);

    }

    public static function date_range($first, $last, $step = '+1 day', $output_format = 'Y-m-d' ) {

        $dates = array();
        $current = strtotime($first);
        $last = strtotime($last);

        while( $current <= $last ) {

            $dates[] = date($output_format, $current);
            $current = strtotime($step, $current);
        }

        return $dates;
    }


}
