<?php

namespace Models;

class Members_health_credentials extends Basemodel
{

    public function initialize()
    {
        $this->belongsTo(
            'membersid',
            'Models\Members',
            'membersid',
            [
                "alias" => "memberHpcredential",
            ]
        );
    }
    public function beforeValidationOnCreate()
    {
        $this->datecreated = date('Y-m-d H:i:s');
        $this->dateupdated = date('Y-m-d H:i:s');
    }

    public function beforeUpdate()
    {
        $this->dateupdated = date('Y-m-d H:i:s');
    }

    public static function insertData($data, $memberid)
    {

        $placeholder = [];
        $val         = [];
        $date        = date('Y-m-d H:i:s');
        $qry         = 'INSERT INTO members_health_credentials (memberid, credentials,required,effectiveDate,expirationDate,dateVerified,verifiedBy,status,notes,datecreated, dateupdated) VALUES ';

        foreach ($data as $value) {
            $placeholder[] = '(?, ?, ?, ?,?, ?, ?, ?,?, ?, ?)';
            $val[]         = $memberid;
            $val[]         = $value['credentials'];
            $val[]         = $value['required'];
            $val[]         = $value['effectiveDate'] ? $value['effectiveDate'] : null;
            $val[]         = $value['expirationDate'] ? $value['expirationDate'] : null;
            $val[]         = $value['dateVerified'] ? $value['dateVerified'] : null;
            $val[]         = $value['verifiedBy'];
            $val[]         = $value['status'];
            $val[]         = $value['notes'];
            $val[]         = $date;
            $val[]         = $date;
        }

        $qry .= implode(',', $placeholder);

        // Execute the query
        try {
            $model = new Members_health_credentials();
            $model->getReadConnection()->query($qry, $val);
            return true;
        } catch (\Exception $e) {
            return $e;
        }
    }

    public static function membersHpCred($memberid)
    {
        $modeldata = Members_health_credentials::find('memberid="' . $memberid . '"')->toArray();
        $data      = [];
        foreach ($modeldata as $key => $value) {
            $data[] = array(
                'credentials'        => $value['credentials'],
                'required'           => $value['required'],
                'effectiveDate'      => $value['effectiveDate'],
                'expirationDate'     => $value['expirationDate'],
                'dateVerified'       => $value['dateVerified'],
                'verifiedBy'         => $value['verifiedBy'],
                'status'             => $value['status'],
                'notes'              => $value['notes'],
            );
        }

        return $data;
    }
}
