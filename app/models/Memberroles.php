<?php

namespace Models;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

// Role Contants
use Constants\RolesConstants;

class Memberroles extends Basemodel {

    public $role;

    public function initialize() {
        $this->belongsTo(
                'membersid',
                'Models\Members',
                'membersid',
                [
                    "alias" => "memberinfo"
                ]
            );
    }

    public static function insertMemberRoles($roles,$memberid)
    {

		$placeholder = [];
        $val = [];
        $date = date('Y-m-d H:i:s');
        $qry = 'INSERT INTO memberroles (memberid, role, datecreated, dateedited) VALUES ';

        // Assigning Default Roles
        $r_array = $roles['sys'] ? $roles['sys']:[]; 
        $d_array = RolesConstants::DEFAULTROLES;
        
        $roles['sys'] = array_unique(array_merge($r_array, $d_array));
        //$roles['sys'] = isset($roles['sys']) ? array_merge($roles['sys'], RolesConstants::DEFAULTROLES) : RolesConstants::DEFAULTROLES;
        
        foreach($roles as $role){
           
                foreach($role as $r){
                    $placeholder[] = '(?, ?, ?, ?)';
                    $val[] = $memberid;
                    $val[] = $r;
                    $val[] = $date;
                    $val[] = $date;
                }   
        
        }

        $qry .= implode(',', $placeholder);

        // Execute the query
        try{
        	$model = new Memberroles();
        	$model->getReadConnection()->query($qry, $val);
        	return true;
        }catch(\Exception $e){
        	return $e;
        }
    }

    public static function memberListRoles($memberid, $owner = false)
    {

        $sql = "Select roleCode, roleGroup FROM memberroles INNER JOIN roleitems ON memberroles.role = roleitems.roleCode WHERE memberroles.memberid = ?";
        
        // A raw SQL statement
        if($owner){
            $sql = "Select roleCode, roleGroup FROM roleitems";
        }



        // Base model
        $roles = new Memberroles();

        // Execute the query
        $query = $roles->getReadConnection()->query($sql, [$memberid]);
        $result = new Resultset(null, $roles, $query);

        $data = [];
        if(!empty($result->toArray())){
            foreach ($result->toArray() as $val) {
                $data[$val['roleGroup']][] = $val['roleCode'];
            }
        }
        
        return $data;
    }

}
