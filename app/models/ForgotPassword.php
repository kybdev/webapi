<?php

namespace Models;

use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Phalcon\Db\Column;

class Forgotpassword extends Basemodel {

    public function initialize() {
    	
    }
    public static function cancelRequest($username, $email){
		// A raw SQL statement
        $sql = "UPDATE dbhcare.forgotpassword SET step='DISCONTINUED' WHERE  username = ? AND email = ? AND step = 'IDENTITY'";

        try{
			// Base model
        	$sq = new Forgotpassword();

        	// Execute the query
        	$query = $sq->getReadConnection()->query($sql, [$username,$email]);
            
        	return true;
        }catch(\Exception $e){
        	return $e;
        }
    }

}
