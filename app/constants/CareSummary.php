<?php

namespace Constants;

class CareSummary
{
    const TYPE = [
        "Follow-up Case Conference",
        "30-Day Summary",
        "60-Day Summary",
        "Discharge Instructions",
        "Discharge Summary"
    ];

    const CODE = [
        "case-con",
        "case-30",
        "case-60",
        "case-discharge-ins",
        "case-discharge-sum"
    ];

    const MODELS = [
        self::CODE[0] => [
            [ "model" => "Models\Case_conference", "fk" => "casesummaryid" ],
            [ "model" => "Models\Case_conference_intermed", "fk" => "caseconid" ]
        ],
        self::CODE[1] => [
            [ "model" => "CaseSummary\Models\Case_30_days", "fk" => "casesummaryid" ]
        ],
        self::CODE[2] => [
            [ "model" => "Models\Sixty_days_summary", "fk" => "casesummaryid" ],
            [ "model" => "Models\Sixty_days_summary_services", "fk" => "idsds" ]
        ]
    ];
}
