<?php

namespace Constants;

/**
 * Class ErrorCodes
 *
 * Constants Taken from Redound
 *
 *  https://github.com/redound/phalcon-rest-boilerplate
 *
 */

class PatientEthnicity
{
    const ETH = [
        "1" => " 1 - American Indian or Alaska Native",
        "2" => " 2 - Asian",
        "3" => " 3 - Black",
        "4" => " 4 - Hispanic or Latino",
        "5" => " 5 - Native Hawaiian or Pacific Islander",
        "6" => " 6 - White",
    ];
}
