<?php

namespace Constants;

/**
 * Class ErrorCodes
 *
 * Constants Taken from Redound
 *
 *  
 *
 */

class TimelineConstants
{
    const SHORTDESC = [
        "adm" ,
        "roc" ,
        "rec" ,
        "psyj",
        "psy" ,
        "dis" ,
        "tra" ,
        "f2f" ,
        "poc-soc",
        "poc-roc",
        "poc-rec",
    ];

    const LONGDESC = [
        self::SHORTDESC[0] => "Admission Order",
        self::SHORTDESC[1] => "ROC Order",
        self::SHORTDESC[2] => "REC Order",
        self::SHORTDESC[3] => "Physician Order (Justification)",
        self::SHORTDESC[4] => "Physician Order",
        self::SHORTDESC[5] => "Discharge Order",
        self::SHORTDESC[6] => "Transfer Report",
        self::SHORTDESC[7] => "Physician Certification Face to Face Encounter",
        self::SHORTDESC[8] => "Plan of Care/485 (SOC)",
        self::SHORTDESC[9] => "Plan of Care/485 (ROC)",
        self::SHORTDESC[10] => "Plan of Care/485 (REC)"
    ];
}
