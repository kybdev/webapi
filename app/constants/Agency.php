<?php

namespace Constants;

class Agency
{
    const ET = [
        0 => 'HHA Only',
        1 => 'HHA with Hospice Programs',
        2 => 'Hospice Only'
    ];

    const ER = [
        0 => 'Parent with Branches',
        1 => 'Branch',
        2 => 'Sole Facility'
    ];

    const IT = [
        0 => 'CGS',
        1 => 'Direct/Self Pay (Non-Ecf)',
        2 => 'Medi-Cal CMC Direct',
        3 => 'NGS - National Government Service',
        4 => 'Not applicable (Private Insurance)',
        5 => 'Palmetto GBA'
    ];

   
}
