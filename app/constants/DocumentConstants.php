<?php

namespace Constants;

use Constants\OrderConstants AS OC;

class DocumentConstants
{

    CONST NUMPAGES = [
        OC::CODE[0]     => 1,   //adm
        OC::CODE[1]     => 1,   //roc
        OC::CODE[2]     => 1,   //rec
        OC::CODE[3]     => 1,   //psyf
        OC::CODE[4]     => 1,   //psy
        OC::CODE[5]     => 1,   //dis
        OC::CODE[6]     => 1,   //tra
        OC::CODE[7]     => 1,   //f2f
        OC::CODE[8]     => 1,   //poc-soc
        OC::CODE[9]     => 1,   //poc-roc
        OC::CODE[10]    => 1,  //poc-rec
    ];

    CONST MODULES = [
        "ORDER",
        "MED-RECON",
        "CC_NOTES",
        "CC_MISSED_VISIT",
        "CASE-30",
        "CASE-60",
        "CASE-CON",
        "CASE-DISCHARGE-INS",
        "CASE-DISCHARGE-SUM",
        "TASKDETAILS",
        "MD_ORDERS"
    ];

    CONST FORMTYPE = [
        "adm" => OC::TYPE[0],      
        "roc" => OC::TYPE[1],      
        "rec" => OC::TYPE[2],      
        "psyj" => OC::TYPE[3],     
        "psy" => OC::TYPE[4],      
        "dis" => OC::TYPE[5],      
        "tra" => OC::TYPE[6],      
        "f2f" => OC::TYPE[7],      
        "poc-soc" => OC::TYPE[8],  
        "poc-roc" => OC::TYPE[9],  
        "poc-rec" => OC::TYPE[10],  
        "med-prof" => "Medication Profile",  
        "med-recon" => "Medication Reconciliation"   
    ];
}
