<?php

namespace Constants;

class NavConstants
{
    const SIDEBARNAV = [
        "DASHBOARD" => [
            "title" => "Dashboard",
            "enabled" => true,
            "url" => "dashboard",
            "icon" => "icon-dashboard blue"
        ],
        "PATIENTS" => [
            "title" => "Patient Manager",
            "enabled" => true,
            "url" => "",
            "icon" => "icon-patient red",
            "roles" => ['pm-ae', 'pm-ap', 'pm-apg', 'pm-apr', 'pm-del'],
            "roles_group" => "pm",
            "sub" => [
                "NEW_PATIENT" => [
                    "title" => "New Patient",
                    "enabled" => true,
                    "url" => "addPatient",
                    "roles_group" => "pm",
                    "roles" => ['pm-ae']
                ],
                "ALL_PATIENTS" => [
                    "title" => "All Patient",
                    "enabled" => true,
                    "url" => [
                        "state" => "patientManage",
                        "param" => [ "status" => 'all' ]
                    ],
                    "roles_group" => "pm",
                    "roles" => ['pm-ap', 'pm-apg', 'pm-apr', 'pm-del'],
                ],
                "PM_DIVIDER" => [
                    "title" => "DIV",
                    "enabled" => true,
                    "url" => "",
                    "roles_group" => "pm",
                    "roles" => ['pm-ap', 'pm-apg', 'pm-apr', 'pm-del'],
                ],
                "ADMITTED" => [
                    "title" => "Admitted",
                    "enabled" => true,
                    "url" => [
                        "state" => "patientManage",
                        "param" => [ "status" => 'admitted' ]
                    ],
                    "roles_group" => "pm",
                    "roles" => ['pm-ap', 'pm-apg', 'pm-apr', 'pm-del'],
                ],
                "PENDING" => [
                    "title" => "Pending",
                    "enabled" => true,
                    "url" => [
                        "state" => "patientManage",
                        "param" => [ "status" => 'pending' ]
                    ],
                    "roles_group" => "pm",
                    "roles" => ['pm-ap', 'pm-apg', 'pm-apr', 'pm-del'],
                ],
                "DISCHARGED" => [
                    "title" => "Discharged",
                    "enabled" => true,
                    "url" => [
                        "state" => "patientManage",
                        "param" => [ "status" => 'discharged' ]
                    ],
                    "roles_group" => "pm",
                    "roles" => ['pm-ap', 'pm-apg', 'pm-apr', 'pm-del'],
                ],
                "INPATIENT" => [
                    "title" => "Inpatient",
                    "enabled" => true,
                    "url" => [
                        "state" => "patientManage",
                        "param" => [ "status" => 'inpatient' ]
                    ],
                    "roles_group" => "pm",
                    "roles" => ['pm-ap', 'pm-apg', 'pm-apr', 'pm-del'],
                ],
                "NON_ADMIT" => [
                    "title" => "Non-Admit",
                    "enabled" => true,
                    "url" => [
                        "state" => "patientManage",
                        "param" => [ "status" => 'non-admit' ]
                    ],
                    "roles_group" => "pm",
                    "roles" => ['pm-ap', 'pm-apg', 'pm-apr', 'pm-del'],
                ]
            ]
        ],
        "ORDERS_MANAGER" => [
            "title" => "Order Manager",
            "enabled" => true,
            "url" => "",
            "icon" => "icon-order-manager cyan",
            "roles" => ['om-aao', 'om-ceo', 'om-up'],
            "roles_group" => "om",
            "sub" => [
                // "NEW_ORDER" => [
                //     "title" => "New Order",
                //     "enabled" => true,
                //     "url" => "neworder",
                //     "roles_group" => "om",
                //     "roles" => ['om-ceo'],
                // ],
                "ALL_ORDER" => [
                    "title" => "All Order",
                    "enabled" => true,
                    "url" => "allorders",
                    "roles_group" => "om",
                    "roles" => ['om-aao'],
                ],
                // "UPLOAD_SIGNED_ORDER" => [
                //     "title" => "Upload Signed Order",
                //     "enabled" => true,
                //     "url" => "",
                //     "roles_group" => "om",
                //     "roles" => ['om-up'],
                // ]
            ]
        ],
        "QA_MANAGER" => [
            "title" => "QA Manager",
            "enabled" => true,
            "url" => "qamanager",
            "icon" => "icon-billing",
            "roles" => ['ads-age', 'ads-plg', 'ads-set', 'ads-sl', 'sys-set'],
            "roles_group" => "admsys",
            // "sub" => [
            // ]
        ],
        "ADMINISTRATION" => [
            "title" => "Administration",
            "enabled" => false,
            "url" => "",
            "icon" => "icon-admin orange",
            "roles" => ['adm-hm', 'adm-im', 'adm-pha', 'adm-pm', 'adm-rm'],
            "roles_group" => "adm",
            "sub" => [
                "PHYSICIAN" => [
                    "title" => "Physician",
                    "enabled" => true,
                    "url" => "physicianManage",
                    "roles_group" => "adm",
                    "roles" => ['adm-pm'],
                ],
                "HOSPITAL" => [
                    "title" => "Facility",
                    "enabled" => true,
                    "url" => "hospitalManage",
                    "roles_group" => "adm",
                    "roles" => ['adm-hm'],
                ],
                "EXTERNAL_REFERAL" => [
                    "title" => "Referral",
                    "enabled" => true,
                    "url" => "externalReferalManage",
                    "roles_group" => "adm",
                    "roles" => ['adm-rm'],
                ],
                "INSURANCE" => [
                    "title" => "Insurance",
                    "enabled" => true,
                    "url" => "insuranceManage",
                    "roles_group" => "adm",
                    "roles" => ['adm-im'],
                ],
                "PHARMACY" => [
                    "title" => "Pharmacy",
                    "enabled" => true,
                    "url" => "managePharmacy",
                    "roles_group" => "adm",
                    "roles" => ['adm-pha'],
                ],
                "ANNOUNCEMENTS" => [
                    "title" => "Announcements",
                    "enabled" => true,
                    "url" => "manageAnnouncements",
                    "roles_group" => "admsys",
                    "roles" => ['sys-set'],
                ]
            ],
        ],
        "USERS" => [
            "title" => "Users",
            "enabled" => false,
            "url" => "",
            "icon" => "icon-member green",
            "roles" => ['usr-ae', 'usr-art', 'usr-aui', 'usr-del', 'usr-esi', 'usr-rup'],
            "roles_group" => "usr",
            "sub" => [
                "NEW_USER" => [
                    "title" => "New User",
                    "enabled" => true,
                    "url" => "createmember",
                    "roles_group" => "usr",
                    "roles" => ['usr-ae'],
                ],
                "ALL_USERS" => [
                    "title" => "All Users",
                    "enabled" => true,
                    "url" => "members",
                    "roles_group" => "usr",
                    "roles" => ['usr-ae', 'usr-aui', 'usr-del', 'usr-rup'],
                ],
                "NEW_USERS_ROLE_TEMPLATE" => [
                    "title" => "New User Role Template",
                    "enabled" => true,
                    "url" => "createroletemplate",
                    "roles_group" => "usr",
                    "roles" => ['usr-art'],
                ],
                "ALL_USERS_ROLE_TEMPLATE" => [
                    "title" => "All User Role Template",
                    "enabled" => true,
                    "url" => "roletemplatelist",
                    "roles_group" => "usr",
                    "roles" => ['usr-art'],
                ]
            ],
        ],
        // "REPORTS" => [
        //     "title" => "Users",
        //     "enabled" => false,
        //     "url" => "",

        // ],
        "DOCUMENT_REPORTS" => [
            "title" => "Document Reports",
            "enabled" => false,
            "url" => "",
            "icon" => "icon-data", //zmdi zmdi-assignment zmdi-hc-fw
            "roles" => ['dom-abf', 'dom-adc', 'dom-afu', 'dom-mbf', 'dom-mdc', 'dom-shr'],
            "roles_group" => "doc",
            "sub" => [
                "MY_UPLOADS" => [
                    "title" => "My Uploads",
                    "enabled" => true,
                    "url" => "",
                    "roles_group" => "doc",
                    "roles" => ['dom-afu']
                ],
                "FILES_SHARED_TO_ME" => [
                    "title" => "Files shared to me",
                    "enabled" => true,
                    "url" => "",
                    "roles_group" => "doc",
                    "roles" => ['dom-shr']
                ],
                "BLANK_FORMS" => [
                    "title" => "Blank Forms",
                    "enabled" => true,
                    "url" => "library.blankForms",
                    "roles_group" => "doc",
                    "roles" => ['dom-mbf']
                ],
                "DOCUMENTS" => [
                    "title" => "Documents",
                    "enabled" => true,
                    "url" => "library.documents",
                    "roles_group" => "doc",
                    "roles" => ['dom-mdc']
                ],
                "ADMIN_REPORTS" => [
                    "title" => "Admin Reports",
                    "enabled" => true,
                    "url" => "library.adminReports",
                    "roles_group" => "doc",
                    "roles" => ['dom-mdc']
                ]
            ]
        ],
        "SYSTEM" => [
            "title" => "System",
            "enabled" => false,
            "url" => "",
            "icon" => "icon-system",
            "roles" => ['ads-age', 'ads-plg', 'ads-set', 'ads-sl', 'sys-set'],
            "roles_group" => "admsys",
            "sub" => [
                "SETTINGS" => [
                    "title" => "Settings",
                    "enabled" => true,
                    "url" => "",
                    "roles_group" => "admsys",
                    "roles" => ['ads-set']
                ],
                "AGENCY_INFO" => [
                    "title" => "Agency Info",
                    "enabled" => true,
                    "url" => "agencyInfo",
                    "roles_group" => "admsys",
                    "roles" => ['ads-age']
                ],
                "SYSTEM_LOGS" => [
                    "title" => "System Logs",
                    "enabled" => true,
                    "url" => "",
                    "roles_group" => "admsys",
                    "roles" => ['ads-age']
                ],
                "MY_HISTORY" => [
                    "title" => "My History",
                    "enabled" => true,
                    "url" => "",
                    "roles_group" => "admsys",
                    "roles" => ['ads-sl']
                ]
            ]
        ],
    ];
}
