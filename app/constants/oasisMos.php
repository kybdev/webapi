<?php

namespace Constants;

class oasisMos{



    const formCode = [
        '00' => 'not-mo',
        '01' => 'txt',
        '02' => 'slct',
        '03' => 'txt-chkbx',
        '04' => 'grp-txt',
        '05' => 'date',
        '06' => 'opt',
        '07' => 'date-chkbx',
        '08' => 'multi-chkbx',
        '09' => 'txt-opt',
        '10' => 'opt-date',
        '11' => 'opt-grpOpt',
        '12' => 'opt-grpOpt-np', //no Parent mo
    ];

    const formGrp = [

    ];

    const sectionMos = [
        'clinical' => [
            'M0010' => [
                'formCode' => oasisMos::formCode['01'],
                'xmlKey'   => 'M0010_CCN',
                'model'    => 'M0010'
            ],
            'M0014' => [
                'formCode' => oasisMos::formCode['02'],
                'xmlKey'   => 'M0014_BRANCH_STATE',
                'model'    => 'M0014'
            ],
            'M0016' => [
                'formCode' => oasisMos::formCode['02'],
                'xmlKey'   => 'M0016_BRANCH_ID',
                'model'    => 'M0016'
            ],
            'M0018' => [
                'formCode' => oasisMos::formCode['03'],
                'txtchk' => [
                    ['type' => 'txt', 'xmlKey' => 'M0018_PHYSICIAN_ID', 'model' => 'M0018NPI' ],
                    ['type' => 'chk', 'xmlKey' => 'M0018_PHYSICIAN_UK', 'model' => 'M0018NPIUK']
                ]
            ],
            'M0020'=> [
                'formCode' => oasisMos::formCode['01'],
                'xmlKey'   => 'M0020_PAT_ID',
                'model'    => 'M0020'
            ],
            'M0030' => [
                'formCode' => oasisMos::formCode['01'],
                'xmlKey'   => 'M0030_START_CARE_DT',
                'model'    => 'M0030'
            ],
            'M0032'=> [
                'formCode' => oasisMos::formCode['03'],
                'txtchk' => [
                    ['type' => 'txt', 'xmlKey' => 'M0032_ROC_DT', 'model' => 'M0032' ],
                    ['type' => 'chk', 'xmlKey' => 'M0032_ROC_DT_NA', 'model' => 'M0032na']
                ]
            ],
            'M0040'=> [
                'formCode' => oasisMos::formCode['04'],
                'txtgrp' => [
                    ['type' => 'txt', 'xmlKey' => 'M0040_PAT_FNAME', 'model' => 'M0040Fname' ],
                    ['type' => 'txt', 'xmlKey' => 'M0040_PAT_MI', 'model' => 'M0040Mi' ],
                    ['type' => 'txt', 'xmlKey' => 'M0040_PAT_LNAME', 'model' => 'M0040Lname' ],
                    ['type' => 'txt', 'xmlKey' => 'M0040_PAT_SUFFIX', 'model' => 'M0040Mi' ],
                ]
            ],
            'M0050' => [
                'formCode' => oasisMos::formCode['02'],
                'xmlKey'   => 'M0050_PAT_ST',
                'model'    => 'M0050'
            ],
            'M0060'=> [
                'formCode' => oasisMos::formCode['01'],
                'xmlKey'   => 'M0060_PAT_ZIP',
                'model'    => 'M0060'
            ],
            'M0063' => [
                'formCode' => oasisMos::formCode['03'],
                'txtchk' => [
                    ['type' => 'txt', 'xmlKey' => 'M0063_MEDICARE_NUM', 'model'=>'M0063' ],
                    ['type' => 'chk', 'xmlKey' => 'M0063_MEDICARE_NA' , 'model'=>'M0063na']
                ]
            ],
            'M0064' => [
                'formCode' => oasisMos::formCode['03'],
                'txtchk' => [
                    ['type' => 'txt', 'xmlKey' => 'M0064_SSN', 'model'=>'M0064' ],
                    ['type' => 'chk', 'xmlKey' => 'M0064_SSN_UK', 'model'=>'M0064na']
                ]
            ],
            'M0065'=> [
                'formCode' => oasisMos::formCode['03'],
                'txtchk' => [
                    ['type' => 'txt', 'xmlKey' => 'M0065_MEDICAID_NUM' , 'model'=>'M0065' ],
                    ['type' => 'chk', 'xmlKey' => 'M0065_MEDICAID_NA', 'model'=>'M0065na']
                ]
            ],
            'M0066' => [
                'formCode' => oasisMos::formCode['05'],
                'xmlKey'   => 'M0066_PAT_BIRTH_DT',
                'model'=>'M0066'
            ],
            'M0069' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M0069_PAT_GENDER',
                'model'=>'M0069',
                'option'   => [
                    ['val'=> '1' , 'desc'=> "Male"],
                    ['val'=> '2' , 'desc'=> "Female"],
                ]
            ],
            'M0080' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M0080_ASSESSOR_DISCIPLINE',
                'model'=>'M0080',
                'option'   => [
                    ['val'=> '01' , 'desc'=> "RN"],
                    ['val'=> '02' , 'desc'=> "PT"],
                    ['val'=> '03' , 'desc'=> "SLP/ST"],
                    ['val'=> '04' , 'desc'=> "OT"],
                ]
            ],
            'M0090' => [
                'formCode' => oasisMos::formCode['05'],
                'xmlKey'   => 'M0090_INFO_COMPLETED_DT',
                'model'=>'M0090'
            ],
            'M0100' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M0100_ASSMT_REASON',
                'model'=>'M0100',
                'option'   => [
                    ['val'=> '01' , 'desc'=> "Start of care - further visits planned"],
                    ['val'=> '03' , 'desc'=> "Resumption of care (after inpatient stay)"],
                    ['val'=> '04' , 'desc'=> "Recertification (follow-up) reassessment"],
                    ['val'=> '05' , 'desc'=> "Other follow-up"],
                    ['val'=> '06' , 'desc'=> "Transferred to an inpatient facility - patient not discharged from agency"],
                    ['val'=> '07' , 'desc'=> "Transferred to an inpatient facility - patient discharged from agency"],
                    ['val'=> '08' , 'desc'=> "Death at home"],
                    ['val'=> '09' , 'desc'=> "Discharge from agency"],
                ]
            ],
            'M0102' => [
                'formCode' => oasisMos::formCode['07'],
                'datechk' => [
                    ['type' => 'date', 'xmlKey' => 'M0102_PHYSN_ORDRD_SOCROC_DT', 'model'=>'M0102Date'],
                    ['type' => 'chk', 'xmlKey' => 'M0102_PHYSN_ORDRD_SOCROC_DT_NA' , 'model'=>'M0102Na']
                ]
            ],
            'M0104' => [
                'formCode' => oasisMos::formCode['05'],
                'xmlKey'   => 'M0104_PHYSN_RFRL_DT',
                'model'    =>'M0104'
            ],
            'M0110'  => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M0110_EPISODE_TIMING',
                'model'    =>'M0110',
                'option'   => [
                    ['val'=> '01' , 'desc'=> "Early"],
                    ['val'=> '02' , 'desc'=> "Later"],
                    ['val'=> 'UK' , 'desc'=> "Unknown"],
                    ['val'=> 'NA' , 'desc'=> "Not Applicable: No Medicare case mix group to be defined by this assessment."],
                ]
            ],
            'M1005' => [
                'formCode' => oasisMos::formCode['07'],
                'datechk' => [
                    ['type' => 'date', 'xmlKey' => 'M1005_INP_DISCHARGE_DT', 'model'=>'M1005' ],
                    ['type' => 'chk',  'xmlKey' => 'M1005_INP_DSCHG_UNKNOWN', 'model'=>'M1005Chk']
                ]
            ],
            'M1000' => [
                'formCode' => oasisMos::formCode['08'],
                'model'    =>'M1000',
                'chkgrp' => [
                    ['xmlKey'=> 'M1000_DC_LTC_14_DA' ,         'desc'=> "1 - Long-term nursing facility (NF)"],
                    ['xmlKey'=> 'M1000_DC_SNF_14_DA' ,         'desc'=> "2 - Skilled nursing facility (SNF / TCU)"],
                    ['xmlKey'=> 'M1000_DC_IPPS_14_DA' ,        'desc'=> "3 - Short-stay acute hospital (IPPS)"],
                    ['xmlKey'=> 'M1000_DC_LTCH_14_DA' ,        'desc'=> "4 - Long-term Care Hospital (LTCH)"],
                    ['xmlKey'=> 'M1000_DC_IRF_14_DA' ,         'desc'=> "5 - Inpatient rehabilitation hospital or unit (RF)"],
                    ['xmlKey'=> 'M1000_DC_PSYCH_14_DA' ,       'desc'=> "6 - Psychiatric hospital or unit"],
                    ['xmlKey'=> 'M1000_DC_OTH_14_DA' ,         'desc'=> "7 - Other (specify)"],
                    ['xmlKey'=> 'M1000_DC_NONE_14_DA' ,        'desc'=> "NA - Patient was not discharged from an inpatient facility <b>[Go to M1017]</b>"]
                ]
            ],
            'M0140'=> [
                'formCode' => oasisMos::formCode['08'],
                'model'    =>'M0140',
                'chkgrp' => [
                    ['xmlKey'=> 'M0140_ETHNIC_AI_AN' ,         'desc'=> "1 - American Indian or Alaska Native"],
                    ['xmlKey'=> 'M0140_ETHNIC_ASIAN' ,         'desc'=> "2 - Asian"],
                    ['xmlKey'=> 'M0140_ETHNIC_BLACK' ,         'desc'=> "3 - Black or African-American"],
                    ['xmlKey'=> 'M0140_ETHNIC_HISP' ,          'desc'=> "4 - Hispanic or Latino"],
                    ['xmlKey'=> 'M0140_ETHNIC_NH_PI' ,         'desc'=> "5 - Native Hawaiian or Pacific Islander"],
                    ['xmlKey'=> 'M0140_ETHNIC_WHITE' ,         'desc'=> "6 - White"],
                ]
            ],

            'M1041' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1041_IN_INFLNZ_SEASON',
                'model'    =>'M1041',
                'option'   => [
                    ['val'=> '0' , 'desc'=> "0 - No [Go to M1051 ]"],
                    ['val'=> '1' , 'desc'=> "1 – Yes"],
                ]
            ],
            'M0150'=> [
                'formCode' => oasisMos::formCode['08'],
                'model'    =>'M0150',
                'chkgrp' => [
                    ['xmlKey'=> 'M0150_CPAY_NONE' ,            'desc'=> "0 - None; no charge for current services  "],
                    ['xmlKey'=> 'M0150_CPAY_MCARE_FFS' ,       'desc'=> "1 - Medicare (Traditional fee-for-service)"],
                    ['xmlKey'=> 'M0150_CPAY_MCARE_HMO' ,       'desc'=> "2 - Medicare (HMO/managed care/Advantage plan)"],
                    ['xmlKey'=> 'M0150_CPAY_MCAID_FFS' ,       'desc'=> "3 - Medicaid (Traditional fee-for-service)"],
                    ['xmlKey'=> 'M0150_CPAY_MCAID_HMO' ,       'desc'=> "4 - Medicaid (HMO/managed care)"],
                    ['xmlKey'=> 'M0150_CPAY_WRKCOMP' ,         'desc'=> "5 - Workers' compensation"],
                    ['xmlKey'=> 'M0150_CPAY_TITLEPGMS' ,       'desc'=> "6 - Title programs (For example, Title III, V, or XX)"],
                    ['xmlKey'=> 'M0150_CPAY_OTH_GOVT' ,        'desc'=> "7 - Other government (For example, TriCare, VA)"],
                    ['xmlKey'=> 'M0150_CPAY_PRIV_INS' ,        'desc'=> "8 - Private insurance"],
                    ['xmlKey'=> 'M0150_CPAY_PRIV_HMO' ,        'desc'=> "9 - Private HMO/managed care"],
                    ['xmlKey'=> 'M0150_CPAY_SELFPAY' ,         'desc'=> "10 - Self-pay"],
                    ['xmlKey'=> 'M0150_CPAY_OTHER' ,           'desc'=> "11 - Other"],
                    ['xmlKey'=> 'M0150_CPAY_UK' ,              'desc'=> "UK - Unknown"],
                ]
            ],
            'M1051' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1051_PVX_RCVD_AGNCY',
                'model'    =>'M1051',
                'option'   => [
                    ['val'=> '0' , 'desc'=> "0 - No"],
                    ['val'=> '1' , 'desc'=> "1 - Yes [Go to M1230 ]"],
                ]
            ],
            'M1056'=> [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1056_PVX_RSN_NOT_RCVD_AGNCY',
                'model'    =>'M1056',
                'option'   => [
                    ['val'=> '01' , 'desc'=> "1 - Offered and declined"],
                    ['val'=> '02' , 'desc'=> "2 - Assessed and determined to have medical contraindication(s)"],
                    ['val'=> '03' , 'desc'=> "3 - Not indicated; patient does not meet age/condition guidelines for Pneumococcal Vaccine"],
                    ['val'=> '04' , 'desc'=> "4 - None of the above"],
                ]
            ],
            'M1046' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1046_INFLNZ_RECD_CRNT_SEASON',
                'model'    =>'M1046',
                'option'   => [
                    ['val'=> '01' , 'desc'=> "1 - Yes; received from your agency during this episode of care (SOC/ROC to Transfer/Discharge)"],
                    ['val'=> '02' , 'desc'=> "2 - Yes; received from your agency during a prior episode of care (SOC/ROC to Transfer/Discharge)"],
                    ['val'=> '03' , 'desc'=> "3 - Yes; received from another health care provider (for example, physician, pharmacist)"],
                    ['val'=> '04' , 'desc'=> "4 - No; patient offered and declined"],
                    ['val'=> '05' , 'desc'=> "5 - No; patient assessed and determined to have medical contraindication(s)"],
                    ['val'=> '06' , 'desc'=> "6 - No; not indicated - patient does not meet age/condition guidelines for influenza vaccine"],
                    ['val'=> '07' , 'desc'=> "7 - No; inability to obtain vaccine due to declared shortage"],
                    ['val'=> '08' , 'desc'=> "8 - No; patient did not receive the vaccine due to reasons other than those listed in responses 4 – 7."],
                ]
            ],
        ],
        'diagnosis' => [
            'M1011' => [
                'formCode' => oasisMos::formCode['03'],
                'txtchk' => [
                    ['type' => 'txt', 'xmlKey' => 'M1011_14_DAY_INP1_ICD' ],
                    ['type' => 'txt', 'xmlKey' => 'M1011_14_DAY_INP2_ICD' ],
                    ['type' => 'txt', 'xmlKey' => 'M1011_14_DAY_INP3_ICD' ],
                    ['type' => 'txt', 'xmlKey' => 'M1011_14_DAY_INP4_ICD' ],
                    ['type' => 'txt', 'xmlKey' => 'M1011_14_DAY_INP5_ICD' ],
                    ['type' => 'txt', 'xmlKey' => 'M1011_14_DAY_INP6_ICD' ],
                    ['type' => 'chk', 'xmlKey' => 'M1011_14_DAY_INP_NA']
                ]
            ],
            'M1017' => [
                'formCode' => oasisMos::formCode['03'],
                'txtchk' => [
                    ['type' => 'txt', 'xmlKey' => 'M1017_CHGREG_ICD1' ],
                    ['type' => 'txt', 'xmlKey' => 'M1017_CHGREG_ICD2' ],
                    ['type' => 'txt', 'xmlKey' => 'M1017_CHGREG_ICD3' ],
                    ['type' => 'txt', 'xmlKey' => 'M1017_CHGREG_ICD4' ],
                    ['type' => 'txt', 'xmlKey' => 'M1017_CHGREG_ICD5' ],
                    ['type' => 'txt', 'xmlKey' => 'M1017_CHGREG_ICD6' ],
                    ['type' => 'chk', 'xmlKey' => 'M1017_CHGREG_ICD_NA']
                ]
            ],
            'M1018' => [
                'formCode' => oasisMos::formCode['08'],
                'chkgrp' => [
                    ['xmlKey'=> 'M1018_PRIOR_UR_INCON' ,        'desc'=> "1 - Urinary Incontinence"],
                    ['xmlKey'=> 'M1018_PRIOR_CATH' ,            'desc'=> "2 - Indwelling/Suprapubic catheter"],
                    ['xmlKey'=> 'M1018_PRIOR_INTRACT_PAIN' ,    'desc'=> "3 - Intractable pain"],
                    ['xmlKey'=> 'M1018_PRIOR_IMPR_DECSN' ,      'desc'=> "4 - Impaired decision-making"],
                    ['xmlKey'=> 'M1018_PRIOR_DISRUPTIVE' ,      'desc'=> "5 - Disruptive or socially inappropriate behavior"],
                    ['xmlKey'=> 'M1018_PRIOR_MEM_LOSS' ,        'desc'=> "6 - Memory loss to the extent that supervision required"],
                    ['xmlKey'=> 'M1018_PRIOR_NONE' ,            'desc'=> "7 - None of the above"],
                    ['xmlKey'=> 'M1018_PRIOR_NOCHG_14D' ,       'desc'=> "NA - No inpatient facility discharge and no change in medical or treatment regimen in past 14 days"],
                    ['xmlKey'=> 'M1018_PRIOR_UNKNOWN' ,         'desc'=> "UK - Unknown"]
                ]
            ],
            'M1021'=> [
                'formCode' => oasisMos::formCode['09'],
                'txtOpt' => [
                    [
                        ['type' => 'txt', 'xmlKey' => 'M1021_PRIMARY_DIAG_ICD' ],
                        ['type' => 'opt', 'xmlKey' => 'M1021_PRIMARY_DIAG_SEVERITY']
                    ]
                ]
            ],
            'M1023' => [
                'formCode' => oasisMos::formCode['09'],
                'txtOpt' => [
                    [
                        ['type' => 'txt', 'xmlKey' => 'M1023_OTH_DIAG1_ICD' ],
                        ['type' => 'opt', 'xmlKey' => 'M1023_OTH_DIAG1_SEVERITY']
                    ],
                    [
                        ['type' => 'txt', 'xmlKey' => 'M1023_OTH_DIAG2_ICD' ],
                        ['type' => 'opt', 'xmlKey' => 'M1023_OTH_DIAG2_SEVERITY']
                    ],
                    [
                        ['type' => 'txt', 'xmlKey' => 'M1023_OTH_DIAG3_ICD' ],
                        ['type' => 'opt', 'xmlKey' => 'M1023_OTH_DIAG3_SEVERITY']
                    ],
                    [
                        ['type' => 'txt', 'xmlKey' => 'M1023_OTH_DIAG4_ICD' ],
                        ['type' => 'opt', 'xmlKey' => 'M1023_OTH_DIAG4_SEVERITY']
                    ],
                    [
                        ['type' => 'txt', 'xmlKey' => 'M1023_OTH_DIAG5_ICD' ],
                        ['type' => 'opt', 'xmlKey' => 'M1023_OTH_DIAG5_SEVERITY']
                    ],
                ]
            ],
            'M1025' => [
                'formCode' => oasisMos::formCode['09'],
                'txtOpt' => [
                    [
                        ['type' => 'txt', 'xmlKey' => 'M1025_OPT_DIAG_ICD_A3'],
                        ['type' => 'opt', 'xmlKey' => 'M1025_OPT_DIAG_ICD_A4']
                    ],
                    [
                        ['type' => 'txt', 'xmlKey' => 'M1025_OPT_DIAG_ICD_B3' ],
                        ['type' => 'opt', 'xmlKey' => 'M1025_OPT_DIAG_ICD_B4']
                    ],
                    [
                        ['type' => 'txt', 'xmlKey' => 'M1025_OPT_DIAG_ICD_C3' ],
                        ['type' => 'opt', 'xmlKey' => 'M1025_OPT_DIAG_ICD_C4']
                    ],
                    [
                        ['type' => 'txt', 'xmlKey' => 'M1025_OPT_DIAG_ICD_D3' ],
                        ['type' => 'opt', 'xmlKey' => 'M1025_OPT_DIAG_ICD_D4']
                    ],
                    [
                        ['type' => 'txt', 'xmlKey' => 'M1025_OPT_DIAG_ICD_E3' ],
                        ['type' => 'opt', 'xmlKey' => 'M1025_OPT_DIAG_ICD_E4']
                    ],
                    [
                        ['type' => 'txt', 'xmlKey' => 'M1025_OPT_DIAG_ICD_F3' ],
                        ['type' => 'opt', 'xmlKey' => 'M1025_OPT_DIAG_ICD_F4']
                    ],
                ]
            ],
            'M1028'  => [
                'formCode' => oasisMos::formCode['08'],
                'chkgrp' => [
                    ['xmlKey'=> 'M1028_ACTV_DIAG_PVD_PAD' ,       'desc'=> "1 - Peripheral Vascular Disease (PVD) or Peripheral Arterial Disease (PAD)"],
                    ['xmlKey'=> 'M1028_ACTV_DIAG_DM' ,            'desc'=> "2 - Diabetes Mellitus (DM)"],
                ]
            ],
            'M1030'  => [
                'formCode' => oasisMos::formCode['08'],
                'chkgrp' => [
                    ['xmlKey'=> 'M1030_THH_IV_INFUSION' ,          'desc'=> "1 - Intravenous or infusion therapy (excludes TPN)  "],
                    ['xmlKey'=> 'M1030_THH_PAR_NUTRITION' ,        'desc'=> "2 - Parenteral nutrition (TPN or lipids)  "],
                    ['xmlKey'=> 'M1030_THH_ENT_NUTRITION' ,        'desc'=> "3 - Enteral nutrition (nasogastric, gastrostomy, jejunostomy, or any other artificial entry into the alimentary canal)"],
                    ['xmlKey'=> 'M1030_THH_NONE_ABOVE' ,           'desc'=> "4 - None of the above"],
                ]
            ],
            'M1033'  => [
                'formCode' => oasisMos::formCode['08'],
                'chkgrp' => [
                    ['xmlKey'=> 'M1033_HOSP_RISK_HSTRY_FALLS' ,         'desc'=> "1 – History of falls(2 or more falls - or any fall with an injury - in the past 12 months)"],
                    ['xmlKey'=> 'M1033_HOSP_RISK_WEIGHT_LOSS' ,         'desc'=> "2 – Unintentional weight loss of a total of 10 pounds or more in the past 12 months"],
                    ['xmlKey'=> 'M1033_HOSP_RISK_MLTPL_HOSPZTN' ,       'desc'=> "3 – Multiple hospitalizations(2 or more) in the past 6 months"],
                    ['xmlKey'=> 'M1033_HOSP_RISK_MLTPL_ED_VISIT' ,      'desc'=> "4 – Multiple emergency department visits(2 or more) in the past 6 months"],
                    ['xmlKey'=> 'M1033_HOSP_RISK_MNTL_BHV_DCLN' ,       'desc'=> "5 – Decline in mental, emotional, or behavioral status in the past 3 months"],
                    ['xmlKey'=> 'M1033_HOSP_RISK_COMPLIANCE' ,          'desc'=> "6 – Reported or observed history of difficulty complying with any medical instructions(for example, medications, diet, exercise) in the past 3 months"],
                    ['xmlKey'=> 'M1033_HOSP_RISK_5PLUS_MDCTN' ,         'desc'=> "7 – Currently taking 5 or more medications"],
                    ['xmlKey'=> 'M1033_HOSP_RISK_CRNT_EXHSTN' ,         'desc'=> "8 – Currently reports exhaustion"],
                    ['xmlKey'=> 'M1033_HOSP_RISK_OTHR_RISK' ,           'desc'=> "9 – Other risk(s) not listed in 1 - 8"],
                    ['xmlKey'=> 'M1033_HOSP_RISK_NONE_ABOVE' ,          'desc'=> "10 – None of the above"],
                ]
            ],
            'M1034' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1034_PTNT_OVRAL_STUS',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 – The patient is stable with no heightened risk(s) for serious complications and death (beyond those typical of the patient’s age)."],
                    ['val'=> '01' , 'desc'=> "1 – The patient is temporarily facing high health risk(s) but is likely to return to being stable without heightened risk(s) for serious complications and death (beyond those typical of the patient’s age)."],
                    ['val'=> '02' , 'desc'=> "2 – The patient is likely to remain in fragile health and have ongoing high risk(s) of serious complications and death."],
                    ['val'=> '03' , 'desc'=> "3 – The patient has serious progressive conditions that could lead to death within a year."],
                    ['val'=> 'UK' , 'desc'=> "UK – The patient’s situation is unknown or unclear."],
                ]
            ],
            'M1036'=> [
                'formCode' => oasisMos::formCode['08'],
                'chkgrp' => [
                    ['xmlKey'=> 'M1036_RSK_SMOKING' ,           'desc'=> "1 – Smoking"],
                    ['xmlKey'=> 'M1036_RSK_OBESITY' ,           'desc'=> "2 – Obesity"],
                    ['xmlKey'=> 'M1036_RSK_ALCOHOLISM' ,        'desc'=> "3 – Alcohol dependency"],
                    ['xmlKey'=> 'M1036_RSK_DRUGS' ,             'desc'=> "4 – Drug dependency"],
                    ['xmlKey'=> 'M1036_RSK_NONE' ,              'desc'=> "5 – None of the above"],
                    ['xmlKey'=> 'M1036_RSK_UNKNOWN' ,           'desc'=> "UK – Unknown"],
                ]
            ],
            'M1060'=> [
                'formCode' => oasisMos::formCode['04'],
                'txtgrp' => [
                    ['type' => 'txt', 'xmlKey' => 'M1060_HEIGHT_A' ],
                    ['type' => 'txt', 'xmlKey' => 'M1060_WEIGHT_B' ],
                ]
            ],
        ],
        'general' => [
            'M1100' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1100_PTNT_LVG_STUTN',
                'option'   => [
                    ['val'=> '01' , 'desc'=> "Patient lives alone, around the clock assistance available."],
                    ['val'=> '02' , 'desc'=> "Patient lives alone, regular daytime assistance available."],
                    ['val'=> '03' , 'desc'=> "Patient lives alone, regular nighttime assistance available."],
                    ['val'=> '04' , 'desc'=> "Patient lives alone, occasional / short-term assistance available."],
                    ['val'=> '05' , 'desc'=> "Patient lives alone, no assistance available."],
                    ['val'=> '06' , 'desc'=> "Patient lives with other person(s) in the home, around the clock assistance available."],
                    ['val'=> '07' , 'desc'=> "Patient lives with other person(s) in the home, regular daytime assistance available."],
                    ['val'=> '08' , 'desc'=> "Patient lives with other person(s) in the home, regular nighttime assistance available."],
                    ['val'=> '09' , 'desc'=> "Patient lives with other person(s) in the home, occasional / short-term assistance available."],
                    ['val'=> '10' , 'desc'=> "Patient lives with other person(s) in the home, no assistance available."],
                    ['val'=> '11' , 'desc'=> "Patient lives in congregate situation (for example, assisted living, residential care home), around the clock assistance available."],
                    ['val'=> '12' , 'desc'=> "Patient lives in congregate situation (for example, assisted living, residential care home), regular daytime assistance available."],
                    ['val'=> '13' , 'desc'=> "Patient lives in congregate situation (for example, assisted living, residential care home), regular nighttime assistance available."],
                    ['val'=> '14' , 'desc'=> "Patient lives in congregate situation (for example, assisted living, residential care home), occasional / short-term assistance available."],
                    ['val'=> '15' , 'desc'=> "Patient lives in congregate situation (for example, assisted living, residential care home), no assistance available."],
                ]
            ],
        ],
        'sensory' => [
            'M1200' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1200_VISION',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - Normal Vision: sees adequately in most situations; can see medication labels, newsprint"],
                    ['val'=> '01' , 'desc'=> "1 - Partially impaired: cannot see medication labels or newsprint, but can see obstacles in path, and the surrounding layout; can count fingers at arm's length"],
                    ['val'=> '02' , 'desc'=> "2 - Severely impaired: cannot locate objects without hearing or touching them, or patient nonresponsive."],
                ]
            ],
            'M1210' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1210_HEARG_ABLTY',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - Adequate: hears normal conversation without difficulty."],
                    ['val'=> '01' , 'desc'=> "1 - Usually understands: understands most conversations, but misses some part/intent of message. Requires cues at times to understand"],
                    ['val'=> '02' , 'desc'=> "2 - Severely Impaired: absence of useful hearing."],
                    ['val'=> 'UK' , 'desc'=> "UK - Unable to assess hearing"],
                ]
            ],
            'M1220' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1220_UNDRSTG_VERBAL_CNTNT',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - Understands: clear comprehension without cues or repetitions"],
                    ['val'=> '01' , 'desc'=> "1 - Usually understands: understands most conversations, but misses some part/intent of message. Requires cues at times to understand"],
                    ['val'=> '02' , 'desc'=> "2 - Sometimes understands: understands only basic conversation or simple, direct phrases. Frequently requires cues to understand"],
                    ['val'=> '03' , 'desc'=> "3 - Rarely/Never Understands"],
                    ['val'=> 'UK' , 'desc'=> "UK - Unable to assess understanding"],
                ]
            ],
            'M1230' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1230_SPEECH',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - Expresses complex ideas, feelings, and needs clearly, completely, and easily in all situations with no observable impairment"],
                    ['val'=> '01' , 'desc'=> "1 - Minimal difficulty in expressing ideas and needs (may take extra time; makes occasional errors in word choice, grammar or speech intelligibility; needs minimal prompting or assistance)"],
                    ['val'=> '02' , 'desc'=> "2 - Expresses simple ideas or needs with moderate difficulty(needs prompting or assistance, errors in word choice, organization or speech intelligibility). Speaks in phrases or short sentences"],
                    ['val'=> '03' , 'desc'=> "3 - Has severe difficulty expressing basic ideas or needs and requires maximal assistance or guessing by listener. Speech limited to single words or short phrases"],
                    ['val'=> '04' , 'desc'=> "4 - Unable to express basic needs even with maximal prompting or assistance but is not comatose or unresponsive (for example, speech is nonsensical or "],
                    ['val'=> '05' , 'desc'=> "5 - Patient nonresponsive or unable to speak"],
                ]
            ],
            'M1240' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1240_FRML_PAIN_ASMT',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - No standardized, validated assessment conducted"],
                    ['val'=> '01' , 'desc'=> "1 - Yes, and it does not indicate severe pain"],
                    ['val'=> '02' , 'desc'=> "2 - Yes, and it indicates severe pain"],
                ]
            ],
            'M1242'=> [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1242_PAIN_FREQ_ACTVTY_MVMT',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - Patient has no pain"],
                    ['val'=> '01' , 'desc'=> "1 - Patient has pain that does not interfere with activity or movement"],
                    ['val'=> '02' , 'desc'=> "2 - Less Often than daily"],
                    ['val'=> '03' , 'desc'=> "3 - Daily, but not constantly"],
                    ['val'=> '04' , 'desc'=> "4 - All of the time"],
                ]
            ],
        ],
        'integumentary' => [
            'M1300' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1300_PRSR_ULCR_RISK_ASMT',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - No assessment conducted [Go to M1306]"],
                    ['val'=> '01' , 'desc'=> "1 - Yes, based on an evaluation of clinical factors (for example, mobility, incontinence, nutrition) without use of standardized tool"],
                    ['val'=> '02' , 'desc'=> "2 - Yes, using a standardized, validated tool (for example, Braden, Norton Scale)"],
                ]
            ],
            'M1302' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1302_RISK_OF_PRSR_ULCR',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - No"],
                    ['val'=> '01' , 'desc'=> "1 - Yes"],
                ]
            ],
            'M1306' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1302_RISK_OF_PRSR_ULCR',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - No [Go to M1322]"],
                    ['val'=> '01' , 'desc'=> "1 - Yes"],
                ]
            ],
            'M1307' => [
                'formCode' => oasisMos::formCode['10'],
                'xmlKey'   => 'M1307_OLDST_STG2_AT_DSCHRG',
                'option'   => [
                    ['val'=> '01' , 'desc'=> "1 - Was present at the most recent SOC/ROC assessment"],
                    ['val'=> '02' , 'grp'=> [
                        'desc' => "2 - Developed since the most recent SOC/ROC assessment. Record date pressure ulcer first identified:",
                        'xmlKey' => 'M1307_OLDST_STG2_ONST_DT'
                    ]],
                    ['val'=> 'NA' , 'desc'=> "NA - No Stage 2 pressure ulcers are present at discharge"],
                ]
            ],
            'M1311' => [
                'formCode' => oasisMos::formCode['04'],
                'txtgrp' => [
                    ['type' => 'txt', 'xmlKey' => 'M1311_NBR_PRSULC_STG2_A1' ],
                    ['type' => 'txt', 'xmlKey' => 'M1311_NBR_ULC_SOCROC_STG2_A2' ],
                    ['type' => 'txt', 'xmlKey' => 'M1311_NBR_PRSULC_STG3_B1' ],
                    ['type' => 'txt', 'xmlKey' => 'M1311_NBR_ULC_SOCROC_STG3_B2'],
                    ['type' => 'txt', 'xmlKey' => 'M1311_NBR_PRSULC_STG4_C1'],
                    ['type' => 'txt', 'xmlKey' => 'M1311_NBR_ULC_SOCROC_STG4_C2'],
                    ['type' => 'txt', 'xmlKey' => 'M1311_NSTG_DRSG_D1'],
                    ['type' => 'txt', 'xmlKey' => 'M1311_NSTG_DRSG_SOCROC_D2'],
                    ['type' => 'txt', 'xmlKey' => 'M1311_NSTG_CVRG_E1'],
                    ['type' => 'txt', 'xmlKey' => 'M1311_NSTG_CVRG_SOCROC_E2'],
                    ['type' => 'txt', 'xmlKey' => 'M1311_NSTG_DEEP_TSUE_F1'],
                    ['type' => 'txt', 'xmlKey' => 'M1311_NSTG_DEEP_TSUE_SOCROC_F2'],
                ]
            ],
            'M1313' => [
                'formCode' => oasisMos::formCode['04'],
                'txtgrp' => [
                    ['type' => 'txt', 'xmlKey' => 'M1313_NW_WS_PRSULC_STG2_A' ],
                    ['type' => 'txt', 'xmlKey' => 'M1313_NW_WS_PRSULC_STG3_B' ],
                    ['type' => 'txt', 'xmlKey' => 'M1313_NW_WS_PRSULC_STG4_C' ],
                    ['type' => 'txt', 'xmlKey' => 'M1313_NW_WS_PRSULC_NSTG_DRSG_D'],
                    ['type' => 'txt', 'xmlKey' => 'M1313_NW_WS_PRSULC_NSTG_CVRG_E'],
                    ['type' => 'txt', 'xmlKey' => 'M1313_NW_WS_PRSULC_NSTG_TSUE_F'],
                ]
            ],
            'M1320' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1320_STUS_PRBLM_PRSR_ULCR',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - Newly epithelialized"],
                    ['val'=> '01' , 'desc'=> "1 - Fully granulating"],
                    ['val'=> '02' , 'desc'=> "2 - Early/partial granulation"],
                    ['val'=> '03' , 'desc'=> "3 - Not healing"],
                    ['val'=> 'NA' , 'desc'=> "NA - No observable pressure ulcer"],
                ]
            ],
            'M1322' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1322_NBR_PRSULC_STG1',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0"],
                    ['val'=> '01' , 'desc'=> "1"],
                    ['val'=> '02' , 'desc'=> "2"],
                    ['val'=> '03' , 'desc'=> "3"],
                    ['val'=> '04' , 'desc'=> "4 or more"],
                ]
            ],
            'M1324' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1324_STG_PRBLM_ULCER',
                'option'   => [
                    ['val'=> '01' , 'desc'=> "1 - Stage 1"],
                    ['val'=> '02' , 'desc'=> "2 - Stage 2"],
                    ['val'=> '03' , 'desc'=> "3 - Stage 3"],
                    ['val'=> '04' , 'desc'=> "4 - Stage 4"],
                    ['val'=> 'NA' , 'desc'=> "NA - Patient has no pressure ulcers or no stageable pressure ulcers"],
                ]
            ],
            'M1330' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1330_STAS_ULCR_PRSNT',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - No [Go to M1340]"],
                    ['val'=> '01' , 'desc'=> "1 - Yes, patient has both observable and unobservable stasis ulcers"],
                    ['val'=> '02' , 'desc'=> "2 - Yes, patient has observable stasis ulcers only"],
                    ['val'=> '03' , 'desc'=> "3 - Yes, patient has unobservable stasis ulcers only (known but not observable due to non-removable dressing/device) [Go to M1340]"],
                ]
            ],
            'M1332' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1332_NBR_STAS_ULCR',
                'option'   => [
                    ['val'=> '01' , 'desc'=> "1 - One"],
                    ['val'=> '02' , 'desc'=> "2 - Two"],
                    ['val'=> '03' , 'desc'=> "3 - Three"],
                    ['val'=> '04' , 'desc'=> "4 - Four or more"]
                ]
            ],
            'M1334' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1334_STUS_PRBLM_STAS_ULCR',
                'option'   => [
                    ['val'=> '01' , 'desc'=> "1 - Fully granulating"],
                    ['val'=> '02' , 'desc'=> "2 - Early/Partial granulation"],
                    ['val'=> '03' , 'desc'=> "3 - Not Healing"]
                ]
            ],
            'M1340' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1340_SRGCL_WND_PRSNT',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - No [At SOC/ROC, go to M1350 ; At FU//DC, go to M1400]"],
                    ['val'=> '01' , 'desc'=> "1 - Yes, patient has at least one observable surgical wound"],
                    ['val'=> '02' , 'desc'=> "2 - Surgical wound known but not observable due to non-removable dressing/device [At SOC/ROC, go to M1350 ; At FU/DC, go to M1400]"],
                ]
            ],
            'M1342' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1342_STUS_PRBLM_SRGCL_WND',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - Newly epithelialized"],
                    ['val'=> '01' , 'desc'=> "1 - Fully granulating"],
                    ['val'=> '02' , 'desc'=> "2 - early/partial granulation"],
                    ['val'=> '03' , 'desc'=> "3 - Not healing"],
                ]
            ],
            'M1350' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1342_STUS_PRBLM_SRGCL_WND',
                'option'   => [
                    ['val'=> '0' , 'desc'=> "0 - No"],
                    ['val'=> '1' , 'desc'=> "1 - Yes"],
                ]
            ],
        ],
        'cardio' => [
            'M1400' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1400_WHEN_DYSPNEIC',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - Patient is not short of breath"],
                    ['val'=> '01' , 'desc'=> "1 - When walking more than 20 feet, climbing stairs"],
                    ['val'=> '02' , 'desc'=> "2 - With moderate exertion (for example, while dressing, using commode or bedpan, walking distance less than 20 feet)"],
                    ['val'=> '03' , 'desc'=> "3 - With minimal exertion (for example, while eating, talking, or performing other ADLs) or with agitation"],
                    ['val'=> '04' , 'desc'=> "4 - At rest (during day or night)"],
                ]
            ],
            'M1410' => [
                'formCode' => oasisMos::formCode['08'],
                'chkgrp' => [
                    ['xmlKey'=> 'M1410_RESPTX_OXYGEN' ,         'desc'=> "1 - Oxygen (intermittent or Continuous)"],
                    ['xmlKey'=> 'M1410_RESPTX_VENTILATOR' ,     'desc'=> "2 - Ventilator (continually or at night)"],
                    ['xmlKey'=> 'M1410_RESPTX_AIRPRESS' ,       'desc'=> "3 - Continuous / Bi-level positive airway pressure"],
                    ['xmlKey'=> 'M1410_RESPTX_NONE' ,           'desc'=> "4 - none of the above"]
                ]
            ],
            'M1501' => [
                'formCode' => oasisMos::formCode['08'],
                'chkgrp' => [
                    ['xmlKey'=> 'M1511_HRT_FAILR_NO_ACTN' ,         'desc'=> "0 - No action taken"],
                    ['xmlKey'=> 'M1511_HRT_FAILR_PHYSN_CNTCT' ,     'desc'=> "1 - Patient’s physician (or other primary care practitioner) contacted the same day"],
                    ['xmlKey'=> 'M1511_HRT_FAILR_ER_TRTMT' ,        'desc'=> "2 - Patient advised to get emergency treatment (for example, call 911 or go to emergency room)"],
                    ['xmlKey'=> 'M1511_HRT_FAILR_PHYSN_TRTMT' ,     'desc'=> "3 - Implemented physician-ordered patient-specific established parameters for treatment"],
                    ['xmlKey'=> 'M1511_HRT_FAILR_CLNCL_INTRVTN' ,   'desc'=> "4 - Patient education or other clinical interventions"],
                    ['xmlKey'=> 'M1511_HRT_FAILR_CARE_PLAN_CHG' ,   'desc'=> "5 - Obtained change in care plan orders (for example, increased monitoring by agency, change in visit frequency, telehealth)"],
                ]
            ]
        ],
        'elimination' => [
            'M1600' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1600_UTI',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - No"],
                    ['val'=> '01' , 'desc'=> "1 - Yes"],
                    ['val'=> 'NA' , 'desc'=> "NA - Patient on prophylactic treatment"],
                    ['val'=> 'UK' , 'desc'=> "UK - Unknown"]
                ]
            ],
            'M1610' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1610_UR_INCONT',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - No incontinence or catheter (includes anuria or ostomy for urinary drainage) [Go to M1620]"],
                    ['val'=> '01' , 'desc'=> "1 - Patient is incontinent"],
                    ['val'=> '02' , 'desc'=> "2 - Patient requires a urinary catheter (specifically: external, indwelling, intermittent, or Suprapubic) [Go to M1620]"]
                ]
            ],
            'M1615' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1615_INCNTNT_TIMING',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - Timed-voiding defers incontinence"],
                    ['val'=> '01' , 'desc'=> "1 - Occasional stress incontinence"],
                    ['val'=> '02' , 'desc'=> "2 - During the night only"],
                    ['val'=> '03' , 'desc'=> "3 - During the day only"],
                    ['val'=> '04' , 'desc'=> "4 - During the day and night"],
                ]
            ],
            'M1620' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1620_BWL_INCONT',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - Very rarely or never has bowel incontinence"],
                    ['val'=> '01' , 'desc'=> "1 - Less than once weekly"],
                    ['val'=> '02' , 'desc'=> "2 - One to three times weekly"],
                    ['val'=> '03' , 'desc'=> "3 - Four to six times weekly"],
                    ['val'=> '04' , 'desc'=> "4 - On a daily basis"],
                    ['val'=> '05' , 'desc'=> "5 - More often than once daily"],
                    ['val'=> 'NA' , 'desc'=> "NA - Patient has ostomy for bowel elimination"],
                    ['val'=> 'UK' , 'desc'=> "UK - Unknown"],
                ]
            ],
            'M1630' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1630_OSTOMY',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - Patient does not have an ostomy for bowel elimination."],
                    ['val'=> '01' , 'desc'=> "1 - patient's ostomy was not related to an inpatient stay and did not necessitate change in medical or treatment regimen."],
                    ['val'=> '02' , 'desc'=> "2 - The ostomy was related to an inpatient stay or did necessitate change in medical or treatment regimen."],
                ]
            ],
        ],
        'neuro' => [
            'M1700' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1700_COG_FUNCTION',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - Alert/oriented, able to focus and shift attention, comprehends and recalls task directions independently."],
                    ['val'=> '01' , 'desc'=> "1 - Requires prompting (cuing, repetition, reminders) only under stressful or unfamiliar conditions."],
                    ['val'=> '02' , 'desc'=> "2 - Requires assistance and some direction in specific situation (for example, on all tasks involving shifting of attention) or consistently requires low stimulus environment due to distractibility"],
                    ['val'=> '03' , 'desc'=> "3 - Requires considerable assistance in routine situations. Is not alert and oriented or is unable to shift attention and recall directions more than half the time"],
                    ['val'=> '04' , 'desc'=> "4 - Totally dependent due to disturbances such as constant disorientation, coma , persistent, vegitative state, or delirium."],
                ]
            ],
            'M1710' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1710_WHEN_CONFUSED',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - Never"],
                    ['val'=> '01' , 'desc'=> "1 - In new or complex situations only"],
                    ['val'=> '02' , 'desc'=> "2 - On awakening or at night only"],
                    ['val'=> '03' , 'desc'=> "3 - During the day and evening, but not constantly"],
                    ['val'=> '04' , 'desc'=> "4 - Constantly"],
                    ['val'=> 'NA' , 'desc'=> "NA - Patient nonresponsive"],
                ]
            ],
            'M1720' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1720_WHEN_ANXIOUS',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - None of the time"],
                    ['val'=> '01' , 'desc'=> "1 - Less Often than daily"],
                    ['val'=> '02' , 'desc'=> "2 - Daily, but not constantly"],
                    ['val'=> '03' , 'desc'=> "3 - All of the time"],
                    ['val'=> 'NA' , 'desc'=> "NA - Patient nonresponsive"],
                ]
            ],
            'M1730' => [
                'formCode' => oasisMos::formCode['11'],
                'xmlKey'   => 'M1730_STDZ_DPRSN_SCRNG',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - No"],
                    ['val'=> '01' , 'desc'=> "1 - Yes, patient was screened using the PHQ-2©* scale.",
                        'optgrp'=>[
                            [
                                'xmlKey'   => 'M1730_PHQ2_LACK_INTRST',
                                'option'   => [
                                    ['val'=> '00' , 'desc'=> "0 - 	Not at all / 0-1 day"],
                                    ['val'=> '01' , 'desc'=> "1 - 	Several days / 2-6 days"],
                                    ['val'=> '02' , 'desc'=> "2 - More than half of the days / 7-11 days"],
                                    ['val'=> '03' , 'desc'=> "3 - Nearly every day / 12-14 days"],
                                    ['val'=> 'NA' , 'desc'=> "NA - NA / Unable to respond"],
                                ]
                            ],
                            [
                                'xmlKey'   => 'M1730_PHQ2_DPRSN',
                                'option'   => [
                                    ['val'=> '00' , 'desc'=> "0 - 	Not at all / 0-1 day"],
                                    ['val'=> '01' , 'desc'=> "1 - 	Several days / 2-6 days"],
                                    ['val'=> '02' , 'desc'=> "2 - More than half of the days / 7-11 days"],
                                    ['val'=> '03' , 'desc'=> "3 - Nearly every day / 12-14 days"],
                                    ['val'=> 'NA' , 'desc'=> "NA - NA / Unable to respond"],
                                ]
                            ]
                        ]
                    ],
                    ['val'=> '02' , 'desc'=> "2 - Yes, patient was screened with a different standardized, validated assessment and the patient meets criteria for further evaluation for depression."],
                    ['val'=> '03' , 'desc'=> "3 - Yes, patient was screened with a different standardized, validated assessment and the patient does not meet criteria for further evaluation for depression."]
                ]
            ],
            'M1740' => [
                'formCode' => oasisMos::formCode['08'],
                'chkgrp' => [
                    ['xmlKey'=> 'M1740_BD_MEM_DEFICIT' ,    'desc'=> "1 - Memory deficit: failure to recognize familiar persons/places, inability to recall events of past 24 hours, significant memory loss so that supervision is required"],
                    ['xmlKey'=> 'M1740_BD_IMP_DECISN' ,     'desc'=> "2 - Impaired decision-making: failure to perform usual ADLs or IADLs, inability to appropriately stop activities, jeopardizes safety through actions"],
                    ['xmlKey'=> 'M1740_BD_VERBAL' ,         'desc'=> "3 - Verbal disruption: yelling, threatening, excessive profanity, sexual references, etc."],
                    ['xmlKey'=> 'M1740_BD_PHYSICAL' ,       'desc'=> "4 - Physical aggression: aggressive or combative to self and others(for example, hits self, throws objects, punches, dangerous maneuvers with wheelchair or other objects)"],
                    ['xmlKey'=> 'M1740_BD_SOC_INAPPRO' ,    'desc'=> "5 - Disruptive, infantile, or socially inappropriate behavior (excludes verbal actions)"],
                    ['xmlKey'=> 'M1740_BD_DELUSIONS' ,      'desc'=> "6 - Delusional, hallucinatory, or paranoid behavior"],
                    ['xmlKey'=> 'M1740_BD_NONE' ,           'desc'=> "7 - None of the above behaviors demonstrated."],
                ]
            ],
            'M1745' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1745_BEH_PROB_FREQ',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - Never"],
                    ['val'=> '01' , 'desc'=> "1 - Less than once a month"],
                    ['val'=> '02' , 'desc'=> "2 - Once a month"],
                    ['val'=> '03' , 'desc'=> "3 - Several times each month"],
                    ['val'=> '04' , 'desc'=> "4 - Several times a week"],
                    ['val'=> '05' , 'desc'=> "5 - At least daily"],
                ]
            ],
            'M1750' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1745_BEH_PROB_FREQ',
                'option'   => [
                    ['val'=> '0' , 'desc'=> "0 - No"],
                    ['val'=> '1' , 'desc'=> "1 - Yes"]
                ]
            ],
        ],
        'adl' => [
            'M1800' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1800_CRNT_GROOMING',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - Able to groom self unaided, with or without the use of assistive devices or adapted methods."],
                    ['val'=> '01' , 'desc'=> "1 - Grooming utensils must be placed within reach before able to complete grooming activities."],
                    ['val'=> '02' , 'desc'=> "2 - Someone must assist the patient to groom self"],
                    ['val'=> '03' , 'desc'=> "3 - Patient depends entirely upon someone else for grooming needs"],
                ]
            ],
            'M1810' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1810_CRNT_DRESS_UPPER',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - Able to get clothes out of closets and drawers, put them on and remove them from the upper body without assistance."],
                    ['val'=> '01' , 'desc'=> "1 - Able to dress upper body without assistance if clothing is laid or handed to the patient"],
                    ['val'=> '02' , 'desc'=> "2 - Someone must help the patient put on upper body clothing."],
                    ['val'=> '03' , 'desc'=> "3 - Patient depends entirely upon another person to dress the upper body part."],
                ]
            ],
            'M1820' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1820_CRNT_DRESS_LOWER',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - Able to obtain, put on, and remove clothing and shoes without assistance."],
                    ['val'=> '01' , 'desc'=> "1 - Able to dress lower body without assistance if clothing and shoes are laid out or handed to the patient."],
                    ['val'=> '02' , 'desc'=> "2 - Someone must help the patient put on undergarments, slacks, socks or nylons, and shoes"],
                    ['val'=> '03' , 'desc'=> "3 - Patient depends entirely upon another person to dress the lower body."],
                ]
            ],
            'M1830' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1820_CRNT_DRESS_LOWER',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - Able to bathe self in shower or tub independently, including getting in and out of tub/shower"],
                    ['val'=> '01' , 'desc'=> "1 - With the use of devices, is able to bathe self in shower or tub independently, including getting in and out of the tub/shower"],
                    ['val'=> '02' , 'desc'=> "2 - Able to bathe in shower or tub with the intermittent assistance of another person: <br>
                        (a) for the intermittent supervision or encouragement or reminders, OR <br>
                        (b) to get in and out of the shower or tub, OR <br>
                        (c) for washing difficult to reach areas.
                        "],
                    ['val'=> '03' , 'desc'=> "3 - Able to participate in bathing self in shower or tub, but requires presence of another person throughout the bath for assistance or supervision"],
                    ['val'=> '04' , 'desc'=> "4 - Unable to use the shower or tub, but able to bathe self independently with or without the use of devices at the sink, in chair, or on commode"],
                    ['val'=> '05' , 'desc'=> "5 - Unable to use the shower or tub, but able to participate in bathing self in bed, at the sink, in bedside chair, or on commode, with the assistance or supervision of another person"],
                    ['val'=> '06' , 'desc'=> "6 - Unable to participate effectively in bathing and is bathed totally by another person."],
                ]
            ],
            'M1840' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1840_CRNT_TOILTG',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - Able to get to and from the toilet and transfer independently with or without a device."],
                    ['val'=> '01' , 'desc'=> "1 - When reminded, assisted, or supervised by another person, able to get to and from the toilet and transfer"],
                    ['val'=> '02' , 'desc'=> "2 - Unable to get to and from the toilet but is able to use a bedside commode (with or without assistance)."],
                    ['val'=> '03' , 'desc'=> "3 - Unable to get to and from the toilet or bedside commode but is able to use a bedpan/urinal independently"],
                    ['val'=> '04' , 'desc'=> "4 - Is totally dependent in toileting"],
                ]
            ],
            'M1845' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1845_CRNT_TOILTG_HYGN',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - Able to manage toileting hygiene and clothing management without assistance"],
                    ['val'=> '01' , 'desc'=> "1 - Able to manage toileting hygiene and clothing management without assistance if supplies/implements are laid out for the patient."],
                    ['val'=> '02' , 'desc'=> "2 - Someone must help the patient to maintain toileting hygiene and/or adjust clothing."],
                    ['val'=> '03' , 'desc'=> "3 - Patient depends entirely upon another person to maintain toileting hygiene."],
                ]
            ],
            'M1850' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1850_CRNT_TRNSFRNG',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - Able to independently transfer"],
                    ['val'=> '01' , 'desc'=> "1 - Able to transfer with minimal human assistance or with use of an assistive device."],
                    ['val'=> '02' , 'desc'=> "2 - Able to bear weight and pivot during the transfer process but unable to transfer self."],
                    ['val'=> '03' , 'desc'=> "3 - Unable to transfer self and in unable to bear weight or pivot when transferred by another person."],
                    ['val'=> '04' , 'desc'=> "4 - Bedfast, unable to transfer but is able to turn and position self in bed."],
                    ['val'=> '05' , 'desc'=> "5 - Bedfast, unable to transfer and is unable to turn and position self."],
                ]
            ],
            'GG0170C' =>[
                'formCode' => oasisMos::formCode['00'],
                'txtgrp' => [
                    ['type' => 'txt', 'xmlKey' => 'GG0170C_MOBILITY_SOCROC_PERF' ],
                    ['type' => 'txt', 'xmlKey' => 'GG0170C_MOBILITY_DSCHG_GOAL' ],
                ]
            ],
            'M1860' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1860_CRNT_AMBLTN',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 -Able to independently walk on even and uneven surfaces and negotiate stairs with or without railings (specifically: needs no human assistance or assistive device.)"],
                    ['val'=> '01' , 'desc'=> "1 - With the use of a one-handed device (for example, cane, single crutch, hemi-walker), able to independently walk on even and uneven surfaces and negotiate stairs with or without railings."],
                    ['val'=> '02' , 'desc'=> "2 - Requires use of a two-handed device (for example, walker or crutches) to walk alone on a level surface and/or requires human supervision or assistance to negotiate stairs or steps or uneven surfaces"],
                    ['val'=> '03' , 'desc'=> "3 - Able to walk only with the supervision or assistance of another person at all times."],
                    ['val'=> '04' , 'desc'=> "4 - Chairfast, unable to ambulate but is able to wheel self independently."],
                    ['val'=> '05' , 'desc'=> "5 - Chairfast, unable to ambulate and is unable to wheel self"],
                    ['val'=> '06' , 'desc'=> "6 - Bedfast, unable to ambulate or be up in a chair."],
                ]
            ],
            'M1870' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1870_CRNT_FEEDING',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - Able to independently feed self."],
                    ['val'=> '01' , 'desc'=> "1 - Able to feed self independently but requires: <br>
                        (a) meal set-up; OR <br>
                        (b) intermittent assistance or supervision from another person; OR <br>
                        (c) a liquid, pureed or ground meat diet."],
                    ['val'=> '02' , 'desc'=> "2 - Unable to feed self and must be assisted or supervised throughout the meal/snack."],
                    ['val'=> '03' , 'desc'=> "3 - Able to take in nutrients orally and receives supplemental nutrients through a nasogastric tube or gastrostomy"],
                    ['val'=> '04' , 'desc'=> "4 - Unable to take in nutrients orally and is fed nutrients through a nasogastric tube or gastrostomy."],
                    ['val'=> '05' , 'desc'=> "5 - Unable to take in nutrients orally or by tube feeding."],
                ]
            ],
            'M1880'  => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1880_CRNT_PREP_LT_MEALS',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - (a) Able to independently plan and prepare all light meals for self or reheat delivered meals; <br>
                        OR  <br>
                        (b) Is physically, cognitively, and mentally able to prepare light meals on a regular basis but has not routinely performed light meal preparation in the past (specifically: prior to this home care admission)"],
                    ['val'=> '01' , 'desc'=> "1 - Unable to prepare light meals on a regular basis due to physical, cognitive, or mental limitations."],
                    ['val'=> '02' , 'desc'=> "2 - Unable to prepare any light meals or reheat any delivered meals."],
                ]
            ],
            'M1890' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1890_CRNT_PHONE_USE',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - Able to dial numbers and answer calls appropriately and as desired."],
                    ['val'=> '01' , 'desc'=> "1 - Able to use a specially adapted telephone (For example, large numbers on the dial, teletype phone for the deaf) and call essential numbers."],
                    ['val'=> '02' , 'desc'=> "2 - Able to answer the telephone and carry on normal conversation but has difficulty with placing calls."],
                    ['val'=> '03' , 'desc'=> "3 - Able to answer the telephone only some of the time or is able to carry on only a limited conversation."],
                    ['val'=> '04' , 'desc'=> "4 - Unable to answer the telephone at all but can listen if assisted with equipment."],
                    ['val'=> '05' , 'desc'=> "5 - Totally unable to use the telephone."],
                    ['val'=> 'NA' , 'desc'=> "NA - Patient does not have a telephone"],
                ]
            ],
            'M1900' => [
                'formCode' => oasisMos::formCode['12'],
                'option'   => [
                    [
                        'xmlKey'   => 'M1900_PRIOR_ADLIADL_SELF',
                        'option'   => [
                            ['val'=> '00' , 'desc'=> "0 - Independent"],
                            ['val'=> '01' , 'desc'=> "1 - Needed Some Help"],
                            ['val'=> '02' , 'desc'=> "2 - Dependent"],
                        ]
                    ],
                    [
                        'xmlKey'   => 'M1900_PRIOR_ADLIADL_AMBLTN',
                        'option'   => [
                            ['val'=> '00' , 'desc'=> "0 - Independent"],
                            ['val'=> '01' , 'desc'=> "1 - Needed Some Help"],
                            ['val'=> '02' , 'desc'=> "2 - Dependent"],
                        ]
                    ],
                    [
                        'xmlKey'   => 'M1900_PRIOR_ADLIADL_TRNSFR',
                        'option'   => [
                            ['val'=> '00' , 'desc'=> "0 - Independent"],
                            ['val'=> '01' , 'desc'=> "1 - Needed Some Help"],
                            ['val'=> '02' , 'desc'=> "2 - Dependent"],
                        ]
                    ],
                    [
                        'xmlKey'   => 'M1900_PRIOR_ADLIADL_HSEHOLD',
                        'option'   => [
                            ['val'=> '00' , 'desc'=> "0 - Independent"],
                            ['val'=> '01' , 'desc'=> "1 - Needed Some Help"],
                            ['val'=> '02' , 'desc'=> "2 - Dependent"],
                        ]
                    ],
                ]
            ],
            'M1910' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1910_MLT_FCTR_FALL_RISK_ASMT',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - No"],
                    ['val'=> '01' , 'desc'=> "1 - Yes, and it does not indicate a risk for falls."],
                    ['val'=> '02' , 'desc'=> "2 - Yes, and it does indicate a risk for falls."],
                ]
            ],
        ],
        'masculo' => [
            'M1910' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M1910_MLT_FCTR_FALL_RISK_ASMT',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - No"],
                    ['val'=> '01' , 'desc'=> "1 - Yes, and it does not indicate a risk for falls."],
                    ['val'=> '02' , 'desc'=> "2 - Yes, and it does indicate a risk for falls."],
                ]
            ],
        ],
        'medication' => [
            'M2001' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M2001_DRUG_RGMN_RVW',
                'option'   => [
                    ['val'=> '0' , 'desc'=> "0 - No - No Issues found during review [Got to 2010]"],
                    ['val'=> '1' , 'desc'=> "1 - Yes - Patient found during review"],
                    ['val'=> '9' , 'desc'=> "9 - NA -Patient is not taking any medications [Got to 2040]"],
                ]
            ],
            'M2003' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M2003_MDCTN_FLWP',
                'option'   => [
                    ['val'=> '0' , 'desc'=> "0 - No"],
                    ['val'=> '1' , 'desc'=> "1 - Yes"],
                ]
            ],
            'M2005' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M2005_MDCTN_INTRVTN',
                'option'   => [
                    ['val'=> '0' , 'desc'=> "0 - No"],
                    ['val'=> '1' , 'desc'=> "1 - Yes"],
                    ['val'=> '9' , 'desc'=> "9 - NA – There were no potential clinically significant medication issues identified since SOC/ROC or patient is not taking any medications"],
                ]
            ],
            'M2010' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M2010_HIGH_RISK_DRUG_EDCTN',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - No"],
                    ['val'=> '01' , 'desc'=> "1 - Yes"],
                    ['val'=> 'NA' , 'desc'=> "NA - Patient not taking any high-risk drugs OR patient/caregiver fully knowledgeable about special precautions associated with all high-risk medications"],
                ]
            ],
            'M2016' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M2016_DRUG_EDCTN_INTRVTN',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - No"],
                    ['val'=> '01' , 'desc'=> "1 - Yes"],
                    ['val'=> 'NA' , 'desc'=> "NA - Patient not taking any high-risk drugs OR patient/caregiver fully knowledgeable about special precautions associated with all high-risk medications"],
                ]
            ],
            'M2020' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M2020_CRNT_MGMT_ORAL_MDCTN',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - Able to independently take the correct oral medication(s) and proper dosage(s) at the correct times."],
                    ['val'=> '01' , 'desc'=> "1 - Able to take medication(s) at the correct times if: <br>
                        (a) individual dosages are prepared in advance by another person; OR <br>
                        (b) another person develops a drug diary or chart"],
                    ['val'=> '02' , 'desc'=> "2 - Able to take medication(s) at the correct times if given reminders by another person at the appropriate times"],
                    ['val'=> '03' , 'desc'=> "3 - Unable to take medication unless administered by another person."],
                    ['val'=> 'NA' , 'desc'=> "NA - No oral medications prescribed."],
                ]
            ],
            'M2030' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M2030_CRNT_MGMT_INJCTN_MDCTN',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - Able to independently take the correct medication(s) and proper dosage(s) at the correct times."],
                    ['val'=> '01' , 'desc'=> "1 - Able to take injectable medication(s) at the correct times if: <br>
                        (a) individual syringes are prepared in advance by another person; OR <br>
                        (b) another person develops a drug diary or chart"],
                    ['val'=> '02' , 'desc'=> "2 - Able to take medication(s) at the correct times if given reminders by another person based on the frequency of the injection"],
                    ['val'=> '03' , 'desc'=> "3 - Unable to take injectable medications unless administered by another person."],
                    ['val'=> 'NA' , 'desc'=> "NA - No injectable medications prescribed."],
                ]
            ],
            'M2040' => [
                'formCode' => oasisMos::formCode['12'],
                'option'   => [
                    [
                        'xmlKey'   => 'M2040_PRIOR_MGMT_ORAL_MDCTN',
                        'option'   => [
                            ['val'=> '00' , 'desc'=> "0 - Independent"],
                            ['val'=> '01' , 'desc'=> "1 - Needed Some Help"],
                            ['val'=> '02' , 'desc'=> "2 - Dependent"],
                            ['val'=> 'NA' , 'desc'=> "Not Applicable"],
                        ]
                    ],
                    [
                        'xmlKey'   => 'M2040_PRIOR_MGMT_INJCTN_MDCTN',
                        'option'   => [
                            ['val'=> '00' , 'desc'=> "0 - Independent"],
                            ['val'=> '01' , 'desc'=> "1 - Needed Some Help"],
                            ['val'=> '02' , 'desc'=> "2 - Dependent"],
                            ['val'=> 'NA' , 'desc'=> "Not Applicable"],
                        ]
                    ]
                ]
            ],
        ],
        'caremngmt' => [
            'M2102' => [
                'formCode' => oasisMos::formCode['12'],
                'option'   => [
                    [
                        'xmlKey'   => 'M2102_CARE_TYPE_SRC_ADL',
                        'option'   => [
                            ['val'=> '00' , 'desc'=> "0 - No assistance needed - patient is independent or does not have needs in this area"],
                            ['val'=> '01' , 'desc'=> "1 - Non-agency caregiver(s) currently provide assistance"],
                            ['val'=> '02' , 'desc'=> "2 - Non-agency caregiver(s) need training/supportive services to provide assistance"],
                            ['val'=> '03' , 'desc'=> "3 - Non-agency caregiver(s) are not likely to provide assistance OR it is unclear if they will provide assistance"],
                            ['val'=> '04' , 'desc'=> "4 - Assistance needed, but no non-agency caregiver(s) available"],
                        ]
                    ],
                    [
                        'xmlKey'   => 'M2102_CARE_TYPE_SRC_IADL',
                        'option'   => [
                            ['val'=> '00' , 'desc'=> "0 - No assistance needed - patient is independent or does not have needs in this area"],
                            ['val'=> '01' , 'desc'=> "1 - Non-agency caregiver(s) currently provide assistance"],
                            ['val'=> '02' , 'desc'=> "2 - Non-agency caregiver(s) need training/supportive services to provide assistance"],
                            ['val'=> '03' , 'desc'=> "3 - Non-agency caregiver(s) are not likely to provide assistance OR it is unclear if they will provide assistance"],
                            ['val'=> '04' , 'desc'=> "4 - Assistance needed, but no non-agency caregiver(s) available"],
                        ]
                    ],
                    [
                        'xmlKey'   => 'M2102_CARE_TYPE_SRC_MDCTN',
                        'option'   => [
                            ['val'=> '00' , 'desc'=> "0 - No assistance needed - patient is independent or does not have needs in this area"],
                            ['val'=> '01' , 'desc'=> "1 - Non-agency caregiver(s) currently provide assistance"],
                            ['val'=> '02' , 'desc'=> "2 - Non-agency caregiver(s) need training/supportive services to provide assistance"],
                            ['val'=> '03' , 'desc'=> "3 - Non-agency caregiver(s) are not likely to provide assistance OR it is unclear if they will provide assistance"],
                            ['val'=> '04' , 'desc'=> "4 - Assistance needed, but no non-agency caregiver(s) available"],
                        ]
                    ],
                    [
                        'xmlKey'   => 'M2102_CARE_TYPE_SRC_PRCDR',
                        'option'   => [
                            ['val'=> '00' , 'desc'=> "0 - No assistance needed - patient is independent or does not have needs in this area"],
                            ['val'=> '01' , 'desc'=> "1 - Non-agency caregiver(s) currently provide assistance"],
                            ['val'=> '02' , 'desc'=> "2 - Non-agency caregiver(s) need training/supportive services to provide assistance"],
                            ['val'=> '03' , 'desc'=> "3 - Non-agency caregiver(s) are not likely to provide assistance OR it is unclear if they will provide assistance"],
                            ['val'=> '04' , 'desc'=> "4 - Assistance needed, but no non-agency caregiver(s) available"],
                        ]
                    ],
                    [
                        'xmlKey'   => 'M2102_CARE_TYPE_SRC_EQUIP',
                        'option'   => [
                            ['val'=> '00' , 'desc'=> "0 - No assistance needed - patient is independent or does not have needs in this area"],
                            ['val'=> '01' , 'desc'=> "1 - Non-agency caregiver(s) currently provide assistance"],
                            ['val'=> '02' , 'desc'=> "2 - Non-agency caregiver(s) need training/supportive services to provide assistance"],
                            ['val'=> '03' , 'desc'=> "3 - Non-agency caregiver(s) are not likely to provide assistance OR it is unclear if they will provide assistance"],
                            ['val'=> '04' , 'desc'=> "4 - Assistance needed, but no non-agency caregiver(s) available"],
                        ]
                    ],
                    [
                        'xmlKey'   => 'M2102_CARE_TYPE_SRC_SPRVSN',
                        'option'   => [
                            ['val'=> '00' , 'desc'=> "0 - No assistance needed - patient is independent or does not have needs in this area"],
                            ['val'=> '01' , 'desc'=> "1 - Non-agency caregiver(s) currently provide assistance"],
                            ['val'=> '02' , 'desc'=> "2 - Non-agency caregiver(s) need training/supportive services to provide assistance"],
                            ['val'=> '03' , 'desc'=> "3 - Non-agency caregiver(s) are not likely to provide assistance OR it is unclear if they will provide assistance"],
                            ['val'=> '04' , 'desc'=> "4 - Assistance needed, but no non-agency caregiver(s) available"],
                        ]
                    ],
                    [
                        'xmlKey'   => 'M2102_CARE_TYPE_SRC_ADVCY',
                        'option'   => [
                            ['val'=> '00' , 'desc'=> "0 - No assistance needed - patient is independent or does not have needs in this area"],
                            ['val'=> '01' , 'desc'=> "1 - Non-agency caregiver(s) currently provide assistance"],
                            ['val'=> '02' , 'desc'=> "2 - Non-agency caregiver(s) need training/supportive services to provide assistance"],
                            ['val'=> '03' , 'desc'=> "3 - Non-agency caregiver(s) are not likely to provide assistance OR it is unclear if they will provide assistance"],
                            ['val'=> '04' , 'desc'=> "4 - Assistance needed, but no non-agency caregiver(s) available"],
                        ]
                    ],
                ]
            ],
            'M2110' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M2110_ADL_IADL_ASTNC_FREQ',
                'option'   => [
                    ['val'=> '01' , 'desc'=> "1 - At least daily"],
                    ['val'=> '02' , 'desc'=> "2 - Three or more Times per week"],
                    ['val'=> '03' , 'desc'=> "3 - One to two Times per week"],
                    ['val'=> '04' , 'desc'=> "4 - Received, but less often than weekly"],
                    ['val'=> '05' , 'desc'=> "5 - No assistance received"],
                    ['val'=> 'UK' , 'desc'=> "UK - Unknown"],
                ]
            ],
        ],
        'theraphy' => [
            'M2200' => [
                'formCode' => oasisMos::formCode['03'],
                'txtchk' => [
                    ['type' => 'txt', 'xmlKey' => 'M2200_THER_NEED_NBR' ],
                    ['type' => 'chk', 'xmlKey' => 'M2200_THER_NEED_NA']
                ]
            ],
            'M2250' => [
                'formCode' => oasisMos::formCode['12'],
                'option'   => [
                    [
                        'xmlKey'   => 'M2250_PLAN_SMRY_PTNT_SPECF',
                        'option'   => [
                            ['val'=> '00' , 'desc'=> "0 - No"],
                            ['val'=> '01' , 'desc'=> "1 - Yes"],
                            ['val'=> 'NA' , 'desc'=> "NA - Not Applicable"]
                        ]
                    ],
                    [
                        'xmlKey'   => 'M2250_PLAN_SMRY_DBTS_FT_CARE',
                        'option'   => [
                            ['val'=> '00' , 'desc'=> "0 - No"],
                            ['val'=> '01' , 'desc'=> "1 - Yes"],
                            ['val'=> 'NA' , 'desc'=> "NA - Not Applicable"]
                        ]
                    ],
                    [
                        'xmlKey'   => 'M2250_PLAN_SMRY_FALL_PRVNT',
                        'option'   => [
                            ['val'=> '00' , 'desc'=> "0 - No"],
                            ['val'=> '01' , 'desc'=> "1 - Yes"],
                            ['val'=> 'NA' , 'desc'=> "NA - Not Applicable"]
                        ]
                    ],
                    [
                        'xmlKey'   => 'M2250_PLAN_SMRY_DPRSN_INTRVTN',
                        'option'   => [
                            ['val'=> '00' , 'desc'=> "0 - No"],
                            ['val'=> '01' , 'desc'=> "1 - Yes"],
                            ['val'=> 'NA' , 'desc'=> "NA - Not Applicable"]
                        ]
                    ],
                    [
                        'xmlKey'   => 'M2250_PLAN_SMRY_PAIN_INTRVTN',
                        'option'   => [
                            ['val'=> '00' , 'desc'=> "0 - No"],
                            ['val'=> '01' , 'desc'=> "1 - Yes"],
                            ['val'=> 'NA' , 'desc'=> "NA - Not Applicable"]
                        ]
                    ],
                    [
                        'xmlKey'   => 'M2250_PLAN_SMRY_PRSULC_PRVNT',
                        'option'   => [
                            ['val'=> '00' , 'desc'=> "0 - No"],
                            ['val'=> '01' , 'desc'=> "1 - Yes"],
                            ['val'=> 'NA' , 'desc'=> "NA - Not Applicable"]
                        ]
                    ],
                    [
                        'xmlKey'   => 'M2250_PLAN_SMRY_PRSULC_TRTMT',
                        'option'   => [
                            ['val'=> '00' , 'desc'=> "0 - No"],
                            ['val'=> '01' , 'desc'=> "1 - Yes"],
                            ['val'=> 'NA' , 'desc'=> "NA - Not Applicable"]
                        ]
                    ],
                ]
            ],
        ],
        'emergent' => [
            'M2301' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M2110_ADL_IADL_ASTNC_FREQ',
                'option'   => [
                    ['val'=> '00' , 'desc'=> "0 - No [Go to M2401]"],
                    ['val'=> '01' , 'desc'=> "1 - Yes, used hospital emergency department WITHOUT hospital admission"],
                    ['val'=> '02' , 'desc'=> "2 - Yes, used hospital emergency department WITH hospital admission"],
                    ['val'=> 'UK' , 'desc'=> "UK - Unknown [Go to M2401]"],
                ]
            ],
            'M2310' => [
                'formCode' => oasisMos::formCode['08'],
                'chkgrp' => [
                    ['xmlKey'=> 'M2310_ECR_MEDICATION' ,         'desc'=> "1 - Improper medication administration, adverse drug reactions, medication side effects, toxicity, anaphylaxis"],
                    ['xmlKey'=> 'M2310_ECR_INJRY_BY_FALL' ,      'desc'=> "2 - Injury caused by fall"],
                    ['xmlKey'=> 'M2310_ECR_RSPRTRY_INFCTN' ,     'desc'=> "3 - Respiratory infection (for example, pneumonia, bronchitis)"],
                    ['xmlKey'=> 'M2310_ECR_RSPRTRY_OTHR' ,       'desc'=> "4 - Other respiratory problem"],
                    ['xmlKey'=> 'M2310_ECR_HRT_FAILR' ,          'desc'=> "5 - Heart failure (for example, fluid overload)"],
                    ['xmlKey'=> 'M2310_ECR_CRDC_DSRTHM' ,        'desc'=> "6 - Cardiac dysrhythmia (irregular heartbeat)"],
                    ['xmlKey'=> 'M2310_ECR_MI_CHST_PAIN' ,       'desc'=> "7 - Myocardial infarction or chest pain"],
                    ['xmlKey'=> 'M2310_ECR_OTHR_HRT_DEASE' ,     'desc'=> "8 - Other heart disease"],
                    ['xmlKey'=> 'M2310_ECR_STROKE_TIA' ,         'desc'=> "9 - Stroke (CVA) or TIA"],
                    ['xmlKey'=> 'M2310_ECR_HYPOGLYC' ,           'desc'=> "10 - Hypo/Hyperglycemia, diabetes out of control"],
                    ['xmlKey'=> 'M2310_ECR_GI_PRBLM' ,           'desc'=> "11 - GI bleeding, obstruction, constipation, impaction"],
                    ['xmlKey'=> 'M2310_ECR_DHYDRTN_MALNTR' ,     'desc'=> "12 - Dehydration, malnutrition"],
                    ['xmlKey'=> 'M2310_ECR_UTI' ,                'desc'=> "13 - Urinary tract infection"],
                    ['xmlKey'=> 'M2310_ECR_CTHTR_CMPLCTN' ,      'desc'=> "14 - IV catheter-related infection or complication"],
                    ['xmlKey'=> 'M2310_ECR_WND_INFCTN_DTRORTN' , 'desc'=> "15 - Wound infection or deterioration"],
                    ['xmlKey'=> 'M2310_ECR_UNCNTLD_PAIN' ,       'desc'=> "16 - Uncontrolled pain"],
                    ['xmlKey'=> 'M2310_ECR_MENTL_BHVRL_PRBLM' ,  'desc'=> "17 - Acute mental/behavioral health problem"],
                    ['xmlKey'=> 'M2310_ECR_DVT_PULMNRY' ,        'desc'=> "18 - Deep vein thrombosis, pulmonary embolus"],
                    ['xmlKey'=> 'M2310_ECR_OTHER' ,              'desc'=> "19 - Other than above reasons"],
                    ['xmlKey'=> 'M2310_ECR_UNKNOWN' ,            'desc'=> "UK - Reason unknown"],
                ]
            ],
            'M2401' => [
                'formCode' => oasisMos::formCode['12'],
                'option'   => [
                    [
                        'xmlKey'   => 'M2102_CARE_TYPE_SRC_ADL',
                        'option'   => [
                            ['val'=> '00' , 'desc'=> "0 - No"],
                            ['val'=> '01' , 'desc'=> "1 - Yes"],
                            ['val'=> 'NA' , 'desc'=> "NA - Not applicable"]
                        ]
                    ],
                    [
                        'xmlKey'   => 'M2401_INTRVTN_SMRY_FALL_PRVNT',
                        'option'   => [
                            ['val'=> '00' , 'desc'=> "0 - No"],
                            ['val'=> '01' , 'desc'=> "1 - Yes"],
                            ['val'=> 'NA' , 'desc'=> "NA - Not applicable"]
                        ]
                    ],
                    [
                        'xmlKey'   => 'M2401_INTRVTN_SMRY_DPRSN',
                        'option'   => [
                            ['val'=> '00' , 'desc'=> "0 - No"],
                            ['val'=> '01' , 'desc'=> "1 - Yes"],
                            ['val'=> 'NA' , 'desc'=> "NA - Not applicable"]
                        ]
                    ],
                    [
                        'xmlKey'   => 'M2401_INTRVTN_SMRY_PAIN_MNTR',
                        'option'   => [
                            ['val'=> '00' , 'desc'=> "0 - No"],
                            ['val'=> '01' , 'desc'=> "1 - Yes"],
                            ['val'=> 'NA' , 'desc'=> "NA - Not applicable"]
                        ]
                    ],
                    [
                        'xmlKey'   => 'M2401_INTRVTN_SMRY_PRSULC_PRVN',
                        'option'   => [
                            ['val'=> '00' , 'desc'=> "0 - No"],
                            ['val'=> '01' , 'desc'=> "1 - Yes"],
                            ['val'=> 'NA' , 'desc'=> "NA - Not applicable"]
                        ]
                    ],
                    [
                        'xmlKey'   => 'M2401_INTRVTN_SMRY_PRSULC_WET',
                        'option'   => [
                            ['val'=> '00' , 'desc'=> "0 - No"],
                            ['val'=> '01' , 'desc'=> "1 - Yes"],
                            ['val'=> 'NA' , 'desc'=> "NA - Not applicable"]
                        ]
                    ],
                ]
            ],
            'M2410' => [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M2410_INPAT_FACILITY',
                'option'   => [
                    ['val'=> '01' , 'desc'=> "1 - Hospital [Go to M2430 ]"],
                    ['val'=> '02' , 'desc'=> "2 - Rehabilitation facility [Go to M0903 ]"],
                    ['val'=> '03' , 'desc'=> "3 - Nursing home [Go to M0903 ]"],
                    ['val'=> '04' , 'desc'=> "4 - Hospice [Go to M0903 ]"],
                    ['val'=> 'NA' , 'desc'=> "NA - No inpatient facility admission"],
                ]
            ],
            'M2420'=> [
                'formCode' => oasisMos::formCode['06'],
                'xmlKey'   => 'M2410_INPAT_FACILITY',
                'option'   => [
                    ['val'=> '01' , 'desc'=> "1 - Patient remained in the community (without formal assistive services)"],
                    ['val'=> '02' , 'desc'=> "2 - Patient remained in the community (with formal assistive services)"],
                    ['val'=> '03' , 'desc'=> "3 - Patient transferred to a non-institutional hospice"],
                    ['val'=> 'UK' , 'desc'=> "UK - Other unknown [Go to M0903 ]"],
                ]
            ],
            'M2430' => [
                'formCode' => oasisMos::formCode['08'],
                'chkgrp' => [
                    ['xmlKey'=> 'M2430_HOSP_MED' ,                'desc'=> "1 - Improper medication administration, adverse drug reactions, medication side effects, toxicity, anaphylaxis "],
                    ['xmlKey'=> 'M2430_HOSP_INJRY_BY_FALL' ,      'desc'=> "2 - Injury caused by fall"],
                    ['xmlKey'=> 'M2430_HOSP_RSPRTRY_INFCTN' ,     'desc'=> "3 - Respiratory infection (for example, pneumonia, bronchitis)"],
                    ['xmlKey'=> 'M2430_HOSP_RSPRTRY_OTHR' ,       'desc'=> "4 - Other respiratory problem"],
                    ['xmlKey'=> 'M2430_HOSP_HRT_FAILR' ,          'desc'=> "5 - Heart failure (for example, fluid overload)"],
                    ['xmlKey'=> 'M2430_HOSP_CRDC_DSRTHM' ,        'desc'=> "6 - Cardiac dysrhythmia (irregular heartbeat)"],
                    ['xmlKey'=> 'M2430_HOSP_MI_CHST_PAIN' ,       'desc'=> "7 - Myocardial infarction or chest pain"],
                    ['xmlKey'=> 'M2430_HOSP_OTHR_HRT_DEASE' ,     'desc'=> "8 - Other heart disease"],
                    ['xmlKey'=> 'M2430_HOSP_STROKE_TIA' ,         'desc'=> "9 - Stroke (CVA) or TIA"],
                    ['xmlKey'=> 'M2430_HOSP_HYPOGLYC' ,           'desc'=> "10 - Hypo/Hyperglycemia, diabetes out of control"],
                    ['xmlKey'=> 'M2430_HOSP_GI_PRBLM' ,           'desc'=> "11 - GI bleeding, obstruction, constipation, impaction"],
                    ['xmlKey'=> 'M2430_HOSP_DHYDRTN_MALNTR' ,     'desc'=> "12 - Dehydration, malnutrition"],
                    ['xmlKey'=> 'M2430_HOSP_UR_TRACT' ,           'desc'=> "13 - Urinary tract infection"],
                    ['xmlKey'=> 'M2430_HOSP_CTHTR_CMPLCTN' ,      'desc'=> "14 - IV catheter-related infection or complication"],
                    ['xmlKey'=> 'M2430_HOSP_WND_INFCTN' ,         'desc'=> "15 - Wound infection or deterioration"],
                    ['xmlKey'=> 'M2430_HOSP_PAIN' ,               'desc'=> "16 - Uncontrolled pain"],
                    ['xmlKey'=> 'M2430_HOSP_MENTL_BHVRL_PRBLM' ,  'desc'=> "17 - Acute mental/behavioral health problem"],
                    ['xmlKey'=> 'M2430_HOSP_DVT_PULMNRY' ,        'desc'=> "18 - Deep vein thrombosis, pulmonary embolus"],
                    ['xmlKey'=> 'M2430_HOSP_SCHLD_TRTMT' ,        'desc'=> "19 - Scheduled treatment or procedure"],
                    ['xmlKey'=> 'M2430_HOSP_OTHER' ,              'desc'=> "20 - Other than above reasons"],
                    ['xmlKey'=> 'M2430_HOSP_UK' ,                 'desc'=> "UK - Reason unknown"],
                ]
            ],
            'M0903' => [
                'formCode' => oasisMos::formCode['05'],
                'xmlKey'   => 'M0903_LAST_HOME_VISIT'
            ],
            'M0906' => [
                'formCode' => oasisMos::formCode['05'],
                'xmlKey'   => 'M0906_DC_TRAN_DTH_DT'
            ],
        ],
    ];
}
