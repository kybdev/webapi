<?php

namespace Controllers;

use Models\Members;
use Models\Memberroles;
use Models\Membersfile;
use Models\Membersdirectory;
use Models\Roletemplate;
use Models\Roleitems;
use Models\Rolegroups;
use Models\Roletemplateitems;
use Models\Membersecurityquestions;
use Models\Securityquestions;
use Models\ForgotPassword;

use Controllers\ControllerBase as CB;
use Controllers\AmazonController as AC;
use Phalcon\Http\Request;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Manager;
use Utilities\Guid\Guid;
use Security\Jwt\JWT;
use Security\AclRoles;
use S3;
use RequestValidator\Member as CMValidator;
use RequestValidator\Esign as Esign;
use RequestValidator\Username;
use RequestValidator\SetSecurity as SS;
use RequestValidator\UserEmail;
use RequestValidator\SSN;
use RequestValidator\RequestResetPW;
use RequestValidator\NewPasswordReset;
use Micro\Exceptions\HTTPExceptions;
use RequestValidator\Custom\Uniquememberdata;

use Constants\RolesConstants;
use Constants\DocStatus;
use Constants\OrderConstants;

use Models\Members_professional_credentials as ProdCred;
use Models\Members_health_credentials as HpCred;

class MembersController extends ControllerBase {

    protected $errors;

    protected $_profileUpdate = false;

    protected $_member = [];

    protected $_memberlanguage = [];

    protected $_constantRole = [
        "a1"
    ];

    protected $attempt = 0;

    public function addMember(){


        $guid = new Guid();


            // $this->validationResponseError(new CMValidator(), $_POST,[
            //     "username" => new Uniquememberdata(['message' => 'Username already taken by another user.','model' => '\Models\Members']),
            //     "email" => new Uniquememberdata(['message' => 'Email already taken by another user.','model' => '\Models\Members']),
            //     "ssn" => new Uniquememberdata(['message' => 'SSN already taken by another user.','model' => '\Models\Members'])
            // ]);


            $password = CB::randomPassword();

            $this->_member['memberid']      = $guid->GUID();
            $this->_member['username']      = $this->request->getPost('serialNumber',['alphanum','trim']);
            $this->_member['email']         = $this->request->getPost('email', ['email','trim']);
            // $this->_member['password']      = $this->security->hash($password);
            $this->_member['password']      = $this->security->hash($this->request->getPost('serialNumber',['alphanum','trim']));
            $this->_member['firstname']     = $this->request->getPost('firstName', ['string','trim']);
            $this->_member['lastname']      = $this->request->getPost('lastName', ['string','trim']);
            $this->_member['suffix']        = $this->request->getPost('suffix', ['string','trim']);
            $this->_member['birthdate']     = $this->request->getPost('birthdate');
            $this->_member['gender']        = $this->request->getPost('gender', ['string','trim']);
            $this->_member['credential']    = $this->request->getPost('credential', ['string','trim'])?:"NA";
            $this->_member['title']         = $this->request->getPost('title', ['string','trim'])?:"NA";
            $this->_member['employeetype']  = $this->request->getPost('employeetype', ['string','trim'])?:"NA";;
            $this->_member['ssn']           = $this->request->getPost('ssn', ['string','trim']);
            $this->_member['employeestatus']= $this->request->getPost('employeestatus', ['string','trim']) ?:"ACTIVE";
            $this->_member['employeeid']    = $this->request->getPost('employeeid', ['string','trim']);
            $this->_member['address1']      = $this->request->getPost('address1', ['string','trim'])?:"NA";
            $this->_member['address2']      = $this->request->getPost('address2', ['string','trim']);
            $this->_member['city']          = $this->request->getPost('city', ['string','trim']) ?:"NA";
            $this->_member['state']         = $this->request->getPost('state', ['string','trim'])?:"NA";
            $this->_member['zip']           = $this->request->getPost('zip', ['string','trim'])?:"NA";
            $this->_member['phone']         = $this->request->getPost('phone', ['string','trim']);
            $this->_member['mobile']        = $this->request->getPost('mobile', ['string','trim']);
            $this->_member['remarks']       = $this->request->getPost('remarks', ['trim']);
            $this->_member['roles']         = $this->request->getPost('roles');
            /*RJ code below*/
            $this->_member['race_ethnicity']= $this->request->getPost('race_ethnicity', ['trim']);
            $this->_member['marital_status']= $this->request->getPost('marital_status', ['trim']);
            $this->_member['discipline']    = $this->request->getPost('discipline', ['trim']);
            $this->_member['fax']           = $this->request->getPost('fax', ['trim']);

            try {

                $savemember = new Members();
                $this->db->begin();

                if(!$savemember->save($this->_member)){
                    $this->errors = $this->errorObjectToArray($savemember->getMessages());
                    $this->db->rollback('Cannot save member.');
                }



                #*** TEMPORARY DEFAULT ROLES
                $this->_member['roles'] = array(
                    "sys"=> [
                        "sys-mes",
                        "sys-pre",
                        "sys-set"
                    ],
                    "admsys"=> [
                        "ads-set",
                        "ads-age",
                        "ads-sl",
                        "ads-plg"
                    ]
                );

               

                $this->_member['roles']['auth'] = $this->_constantRole;
                $res = Memberroles::insertMemberRoles($this->_member['roles'], $this->_member['memberid']);
                if(is_object($res)){
                    $this->errors = $res->getMessage();
                    $this->db->rollback();
                }
                //
                // //send account information to user via email
                // $emailcontent = array(
                //     'username' => $this->_member['username'],
                //     'password' => $password
                // );
                //
                // $mail = $this->Mailer->sendMail($this->_member['email'], 'Medisource :Account Login Information', $emailcontent, 'addTemplate');
                //
                // if($mail == 0){
                //     $this->db->rollback("Email server does not accept your request.");
                // }
                //
                // //save data to database if there is no error

                $this->db->commit();

                //
                // //Generate Role Cache for User
                $this->generateUserCacheAcl($this->_member['memberid']);

                //success message
                return array('Member have been saved.');
            }
            catch (\Exception $e) {
                $this->ErrorMessage->thrower(
                    406,
                    $e->getMessage(),
                    "Saving Errors",
                    "MEM002",
                    $this->errors
                );
            }
    } //end of addMember fucntion

    public function generateUserCacheAcl($memberid){
        $collections = $this->collections->getCollections();
        return AclRoles::singleUserAcl($memberid, $collections, '\Models\Memberroles');
    }


    public function memberLogin(){
        if($this->request->isPost()){
            $username   = $this->request->getPost('username');
            $password   = $this->request->getPost('password');
            $data = $this->User->authorizeUser(array("username"=>$username,"password"=>$password), "Members");
        }

        return $data;
    }


    public function memberList($page=0, $count,$keyword){
        if(empty($count)){
            $count=10;
        }
        if(empty($keyword)){
            $keyword=null;
        }
        $memberresults = Members::listall($page, $count, $keyword);

        return $memberresults;

    }






}
