<?php

namespace Controllers;

use Phalcon\Http\Response;

class ResponseController extends ControllerBase {

	public function respond($response_code = 200, $response_text = "OK", $data = null)
	{
		$response = new Response();
    	$response->setContentType('application/json', 'UTF-8');
    	$response->setStatusCode($response_code, $response_text);

    	if($data) {
    		$data = json_encode($data);
    		$response->setContent($data);
    	}

    	$response->send();
	}

	public function respondData($data)
	{
		self::respond(200, "RESOURCE_LOADED", $data);
	}

	public function respondOk($response_text)
	{
		self::respond(200, $response_text);
	}

	public function respondError($response_code, $response_text)
	{
		self::respond($response_code, $response_text);
	}


}
