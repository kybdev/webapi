<?php

namespace Controllers;

use Controllers\OrderController as OrderCtrl;
use Controllers\MedicationprofController as MedCtrl;

use Constants\DocumentConstants;

class DocumentController extends ControllerBase 
{

    public function get_attachments($type, $foreignkey) {

        $modules = DocumentConstants::MODULES;

        switch (strtoupper($type)) {
            case $modules[0]:
                $ctrl = new OrderCtrl;
                return $ctrl->get_attachments($foreignkey);
                break;
            case $modules[1]:
                $data = array();
                $data['orderType'] = "Medication Reconciliation";
                $ctrl = new MedCtrl;
                $result = $ctrl->get_recon_attachments($foreignkey);

                return array_merge($data, $result);
                break;
            case $modules[2]:
            case $modules[3]:
            case $modules[4]:
            case $modules[5]:
            case $modules[6]:
            case $modules[7]:
            case $modules[8]:
            case $modules[9]:
            case $modules[10]:
                $filenames = [
                    'cc_notes' => "Care Coordination Notes",
                    'cc_missed_visit' => "Care Coordination Missed Visit",
                    'case-30' => "Care Summary - 30 Days",
                    'case-60' => "Care Summary - 60 Days",
                    'case-con' => "Care Summary - Case Conference",
                    'case-discharge-ins' => "Care Summary - Discharge Instruction",
                    'case-discharge-sum' => "Care Summary - Discharge Summary",
                    'taskdetails' => "Task Details",
                    'md_orders' => "MD Orders",
                ];

                $query  = " SELECT patient_record_file.*, cc_attachments.sequence FROM cc_attachments";
                $query .= " LEFT JOIN patient_record_file ON cc_attachments.file_id = patient_record_file.id";
                $query .= " WHERE module = :module AND module_id = :module_id";
                $query .= " ORDER BY sequence ASC";

                $params = [
                    ['keyword' => ':module', 'value' => $type, 'type' => 'str'],
                    ['keyword' => ':module_id', 'value' => $foreignkey, 'type' => 'str'],
                ];

                $data = array( "orderType" => $filenames[$type]);
                $data['patient'] = "";
                $files = $this->customQuery($query, $params);

                foreach($files as $file) {
                    $file['directory'] = $file['path'];
                    $data['attachments'][] = $file;
                }

                return $data;
                break;
            default:
                # code...
                break;
        }

    }

    public function document_zip($type, $foreignkey) {

        $modules = DocumentConstants::MODULES;

        switch (strtoupper($type)) {
            case $modules[0]:
                $ctrl = new OrderCtrl;
                return $ctrl->document_zip($foreignkey);
                break;
            case $modules[1]:
                $data = array();
                $data['orderType'] = "Medication Reconciliation";
                $ctrl = new MedCtrl;
                $result = $ctrl->document_recon_zip($foreignkey);

                return array_merge($data, $result);
                break;
            case $modules[2]:
            case $modules[3]:
            case $modules[4]:
            case $modules[5]:
            case $modules[6]:
            case $modules[7]:
            case $modules[8]:
            case $modules[9]:
            case $modules[10]:
                $filenames = [
                    'cc_notes' => "Care Coordination Notes",
                    'cc_missed_visit' => "Care Coordination Missed Visit",
                    'case-30' => "Care Summary - 30 Days",
                    'case-60' => "Care Summary - 60 Days",
                    'case-con' => "Care Summary - Case Conference",
                    'case-discharge-ins' => "Care Summary - Discharge Instruction",
                    'case-discharge-sum' => "Care Summary - Discharge Summary",
                    'taskdetails' => "Task Details",
                    'md_orders' => "MD Orders",
                ];

                $query  = " SELECT patient_record_file.*, cc_attachments.sequence FROM cc_attachments";
                $query .= " LEFT JOIN patient_record_file ON cc_attachments.file_id = patient_record_file.id";
                $query .= " WHERE module = :module AND module_id = :module_id";
                $query .= " ORDER BY sequence ASC";

                $params = [
                    ['keyword' => ':module', 'value' => $type, 'type' => 'str'],
                    ['keyword' => ':module_id', 'value' => $foreignkey, 'type' => 'str'],
                ];

                $data = array( "filename" => $filenames[$type] . ' - ' . date('Y-m-d') . '.zip');
                $files = $this->customQuery($query, $params);

                foreach($files as $file) {
                    $file['directory'] = $file['path'];
                    $data['files'][] = $file;
                }

                if($type === 'md_orders') {
                    $ctrl = new OrderCtrl;
                    $d = $ctrl->document_zip($foreignkey);
                    $data['filename'] = $d['filename'];
                }

                return $data;
                break;
            default:
                # code...
                break;
        }

    }

}
