<?php

namespace Controllers;

use Phalcon\Http\Request;
use Models\Miscellaneous;
use Models\MiscComment;
use Models\Members;

use Phalcon\Paginator\Adapter\NativeArray as PaginatorArray;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;
use Utilities\Guid\Guid;

class MiscController extends ControllerBase {

	public function get_all_misc() {
		$request = new Request();
        $idpatient = $request->get('idpatient');
        $idpatientepisode = $request->get('idpatientepisode');
		$limit = $request->get('limit') == 'ALL' ? 2147483647 : $request->get('limit');
		$page = $request->get('page');
		$keyword = $request->get('keyword');
		$filter = $request->get('filter');
		$sort = $request->get('sort');

		$filter['from'] = $filter['from'] ? $filter['from'] : '0001-01-01';
		$filter['to'] = $filter['to'] ? $filter['to'] : '9999-12-31';

		$query  = " SELECT miscellaneous.*, 'miscellaneous' as module FROM miscellaneous";
		$query .= " WHERE patientid = :idpatient AND patientepisodeid = :idpatientepisode AND date_assigned BETWEEN :filter_from AND :filter_to";
		$query .= $keyword ? " AND title LIKE '%$keyword%'" : "";
		$query .= " ORDER BY ".$sort['model']." ".$sort['sortBy'];

		$params = [
            ['keyword' => ':idpatient', 'value' => $idpatient, 'type' => 'str'],
            ['keyword' => ':idpatientepisode', 'value' => $idpatientepisode, 'type' => 'str'],
			['keyword' => ':filter_from', 'value' => $filter['from'], 'type' => 'str'],
			['keyword' => ':filter_to', 'value' => $filter['to'], 'type' => 'str'],
		];

		$paginator = new PaginatorArray(
		    [
		        "data"  => $this->customQuery($query, $params),
		        "limit" => $limit,
		        "page"  => $page,
		    ]
		);

		$data = $paginator->getPaginate();
		return $data;
	}

	public function get_all_others() {
		$request = new Request();
        $idpatient = $request->get('idpatient');
        $idpatientepisode = $request->get('idpatientepisode');
		$limit = $request->get('limit') == 'ALL' ? 2147483647 : $request->get('limit');
		$page = $request->get('page');
		$keyword = $request->get('keyword');
		$filter = $request->get('filter');
		$sort = $request->get('sort');

		$filter['from'] = $filter['from'] ? $filter['from'] : '0001-01-01';
		$filter['to'] = $filter['to'] ? $filter['to'] : '9999-12-31';

		$query  = " SELECT patientepisodetaskmissed.taskid as id, patientepisodetaskmissed.*, patientepisodetask.task, taskitems.taskname as title, orderdate as date_assigned, 'patientepisodetask' as module FROM patientepisodetaskmissed";
		$query .= " LEFT JOIN patientepisodetask ON patientepisodetaskmissed.taskid = patientepisodetask.taskid";
		$query .= " LEFT JOIN taskitems ON taskitems.taskid = patientepisodetask.task";
		$query .= " WHERE patientid = :idpatient AND episodeid = :idpatientepisode AND orderdate BETWEEN :filter_from AND :filter_to";
		$query .= " ORDER BY ".$sort['model']." ".$sort['sortBy'];

		$params = [
            ['keyword' => ':idpatient', 'value' => $idpatient, 'type' => 'str'],
            ['keyword' => ':idpatientepisode', 'value' => $idpatientepisode, 'type' => 'str'],
			['keyword' => ':filter_from', 'value' => $filter['from'], 'type' => 'str'],
			['keyword' => ':filter_to', 'value' => $filter['to'], 'type' => 'str'],
		];

		$paginator = new PaginatorArray(
		    [
		        "data"  => $this->customQuery($query, $params),
		        "limit" => $limit,
		        "page"  => $page,
		    ]
		);

		$data = $paginator->getPaginate();
		return $data;
	}

	public function get_misc($misc_id) {
		$misc = Miscellaneous::findFirst([
			"id = :misc_id:",
			'bind' => [ 'misc_id' => $misc_id ]
		]);

		return $misc;
	}

	public function create() {
		$request = new Request();

		$misc    = new Miscellaneous();
		$columns = ['id', 'title', 'content', 'date_assigned', 'patientid', 'patientepisodeid'];

		$data 	 	= $request->get();
		$data['id'] = (new Guid())->GUID();

		if(! $misc->save($data, $columns)) {
			return [ 'result' => 'failed' ];
		}

		return [ 'result' => 'success', 'pk' => $misc->id ];
	}

	public function update_misc($misc_id) {
		$request = new Request();

		$misc 	 = Miscellaneous::findFirst([
			"id = :misc_id:",
			'bind' => [ 'misc_id' => $misc_id ]
		]);
		$columns = ['title', 'content', 'date_assigned'];
		$data 	 	= $request->getPut();

		if(! $misc->update($data, $columns)) {
			return [ 'result' => 'failed' ];
		}

		return [ 'result' => 'success' ];
	}

	public function delete_misc() {
		$request = new Request();
		$data = $request->get();

		$items = Miscellaneous::query()->where("id IN ('".implode("', '", $data['item_ids'])."')")->execute();
		if(! $items->delete()) {
			return [ 'result' => 'failed' ];
		}

		return [ 'result' => 'success' ];
	}

	public function insert_comment($mod, $modid) {
		$request = new Request();
		$data = $request->get();

		$comment = new MiscComment();
		$columns = [ 'module', 'moduleid', 'comment', 'memberid' ];

		if(! $comment->save($data, $columns)) {
			return ['result' => 'failed'];
		}

		$member = Members::findFirst([
			"memberid = :memberid:",
			'bind' => [ 'memberid' => $comment->memberid ]
		]);

		$comment->firstname = $member->firstname;
		$comment->lastname = $member->lastname;
		return $comment;
	}

	public function update_comment($id) {
		$request = new Request();
		$data = $request->getPut();

		$comment = MiscComment::findFirst($id);

		if(! $comment->update($data, ['comment'])) {
			return ['result' => 'failed'];
		}

		return $comment;
	}

	public function get_comments($mod, $modid, $page) {
		$query  = " SELECT misc_comments.*, members.firstname, members.lastname FROM misc_comments";
		$query .= " LEFT JOIN members ON members.memberid = misc_comments.memberid";
		$query .= " WHERE module = :module AND moduleid = :module_id";
		$query .= " ORDER BY misc_comments.datecreated DESC";

		$params = [
            ['keyword' => ':module', 'value' => $mod, 'type' => 'str'],
            ['keyword' => ':module_id', 'value' => $modid, 'type' => 'str'],
		];

		$comments = $this->customQuery($query, $params);

		$paginator = new PaginatorArray([
	        "data"  => $comments,
	        "limit" => 5,
	        "page"  => $page,
	    ]);

		return $paginator->getPaginate();
	}

	public function delete_comment($id) {
		$comment = MiscComment::findFirst($id);

		if(! $comment) {
			return ['result' => 'failed'];
		}

		if(! $comment->delete()) {
			return ['result' => 'failed'];
		}

		return ['result' => 'success'];
	}

	public function print_data($misc_id) {
		$data = array();
		$data['note'] = Miscellaneous::findFirst([
			"id = :misc_id:",
			'bind' => [ 'misc_id' => $misc_id ]
		]);
		$date = new \DateTime($data['note']->date_assigned);
		$data['note']->date_assigned = $date->format('m/d/y');
		$data['patient'] = OrderController::doc_patient_info($data['note']->patientid);

		return $data;
	}

}