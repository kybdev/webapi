<?php
namespace Controllers;

// Model
use Models\Patient;

class DashboardController extends ControllerBase {

    protected $errors;

    public function index($memberid, $page, $count, $keyword) {
        $data = array();
        $data['patient'] = Patient::listall($page, $count, $keyword, $this->User->getUser()->id, 'all', 'all', 'all');

        return $data;
    }

    public function fetchadmincounter(){
    	return Patient::fetchadmincounter();
    }

    public function fetchcliniciancounter(){

    	$currentuser = $this->User->getUser();

    	return Patient::fetchcliniciancounter($currentuser);
    }

    public function fetchqacmcounter(){

    	$currentuser = $this->User->getUser();

    	return Patient::fetchqacmcounter($currentuser);
    }

    public function fetchtasks(){
    	if($this->request->isPost()){

    		$postdata = $this->request->getPost();
    		$currentuser = $this->User->getUser();

    		return Patient::dashboardfetchtasks($postdata, $currentuser);

    	}
    }

    public function fetchtasksfilter(){
    	if($this->request->isGet()){
    		$currentuser = $this->User->getUser();

    		return Patient::dashboardfetchtasksfilter($currentuser);

    	}
    }

    public function fetchpatient(){

    	if($this->request->isPost()){

    		$postdata = $this->request->getPost();
    		$currentuser = $this->User->getUser();

    		return Patient::fetchpatient($postdata, $currentuser);

    	}

    }

}