<?php

namespace Controllers;

use Models\Members;
use Models\Memberroles;
use Models\Membersfile;
use Models\Membersdirectory;
use Models\Roletemplate;
use Models\Logininfo;
use Models\Roleitems;
use Models\Membersecurityquestions;
use Controllers\ControllerBase as CB;
use Controllers\AmazonController;
use Phalcon\Http\Request;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Mvc\Model\Transaction\Manager as TxManager;
use Phalcon\Mvc\Model\Manager;
use Utilities\Guid\Guid;
use Security\Jwt\JWT;
use Security\AclRoles;
use S3;
use RequestValidator\Member as CMValidator;
use RequestValidator\Username;
use RequestValidator\UserEmail;
use RequestValidator\SSN;
use Micro\Exceptions\HTTPExceptions;
use RequestValidator\Custom\Uniquememberdata;

class RolesController extends ControllerBase {

    protected $errors;

    protected $_member = [];

    protected $_constantRole = [
        "a1"
    ];

    public function endpoints(){
    	$roleitems = Roleitems::find();
    	$collections = $this->collections->getCollections();

    	if(!$roleitems){
    		$this->ErrorMessage->thrower(
                    406, 
                    "Empty Role Items",
                    "Error roles",
                    "MEM002"
                );
    	}

    	//var_dump($roleitems->toArray());
    	
    	$roleEndpoints = [];

    	foreach ($roleitems as $roles) {
    		
    		$roleEndpoints[$roles->roleCode] = [
    			"roleGroup" => $roles->roleGroup,
    			"roleName" => $roles->roleName,
    		];

    		$endpoints = [];
    		foreach($collections as $col){
            	foreach($col->getHandlers() as $colh) {
            		if($colh[3] == $roles->roleCode){
            			$endpoints[]=$col->getPrefix() . $colh[1];
            		}
            	}
        	}

        	if(!empty($endpoints)){
        		$roleEndpoints[$roles->roleCode]['endpoints'] = $endpoints;
        	}

    	}
    	return $roleEndpoints;
    }

}