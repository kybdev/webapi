<?php

namespace Controllers;

use \Models\Agents as Agents;
use \Models\Agentimage as Agentimage;
use \Controllers\ControllerBase as CB;

class AgentController extends \Phalcon\Mvc\Controller {


    public function userExistAction($name) {
        if(!empty($name)) {
            $agent = Agents::findFirst('username="' . $name . '"');
            if ($agent) {
                echo json_encode(array('exists' => 'true'));
            } else {
                echo json_encode(array('exists' => 'false'));
            }
        }
    }

    public function emailExistAction($name)
    {
        if (!empty($name)) {
            $agent = Agents::findFirst('email="' . $name . '"');
            if ($agent) {
                echo json_encode(array('exists' => 'true'));
            } else {
                echo json_encode(array('exists' => 'false'));
            }
        }
    }

    public function activationAction()
    {
        if (!empty($_POST['code'])) {
            $code = $_POST['code'];
            $agent = Agents::findFirst('activation_code="' . $code . '"');
            if (isset($agent->activation_code)) {
                $agent->activation_code = null;
                $agent->status = 1;
                if($agent->save()){
                    echo json_encode(array('success' => 'Your account have been fully activated. You can now login.'));
                }else{
                    echo json_encode(array('error' => 'Cannot update record.'));
                }
            } else {
                echo json_encode(array('error' => 'No activation code found. Activation code is expired.'));
            }
        }
    }

    public function registerUserAction($agent){
        $request = new \Phalcon\Http\Request();
        //var_dump($request->getPost('username'));
        if($request->isPost()){
            $agentname = $request->getPost('username');
            $email = $request->getPost('email');
            $password = $request->getPost('password');
            $referalcode = $request->getPost('referalcode');
            $source = $request->getPost('source');
            $newsletter = $request->getPost('newsletter');
            $firstname = $request->getPost('firstname');
            $lastname = $request->getPost('lastname');
            $gender = $request->getPost('gender');
            $country = $request->getPost('country');
            $state = $request->getPost('state');
            $month = $request->getPost('month');
            $day = $request->getPost('day');
            $year = $request->getPost('year');

            /* Register User*/
            $guid = new \Utilities\Guid\Guid();
            $usave = new Agents();
            $usave->id = $guid->GUID();
            $usave->username = $agentname;
            $usave->email = $email;
            $usave->password = sha1($password);
            $usave->referal_code = $referalcode;
            $usave->source_info = $source;
            $usave->subscribe_newsletter = $newsletter;
            $usave->first_name = $firstname;
            $usave->last_name = $lastname;
            $usave->gender = $gender;
            $usave->country = $country;
            $usave->state = $state;
            $usave->status = 0;
            $usave->birthday = $year . '-' . $month . '-' . $day;
            $usave->activation_code = $code = $guid->GUID();
            $usave->created_at = date("Y-m-d H:i:s");
            $usave->updated_at = date("Y-m-d H:i:s");

            if(!$usave->create()){
                $errors = array();
                foreach ($usave->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            }else{
                $app = new CB();
                $content = 'Welcome to Planet Impossible, '.$firstname. ' '. $lastname . ' <br>  <br> Please click the confirmation link below to complete and activate your account.
                <br><br>  Link: <br><a href="' . $app->getConfig()->application->baseURL . '/activation/'.$code.'">' . $app->getConfig()->application->baseURL . '/activation/'.$code.'</a><br><br> <br><br>Thanks,<br><br>PI Staff';
                $app->sendMail($email, 'PI Confirmation Email', $content);
                echo json_encode(array('success' => 'You are now successfuly registered.'));
            }
        }else{
            echo json_encode(array('error' => 'No post data.'));
        }
    }

    public function loginAction() {
        $app = new CB();

        $request = new \Phalcon\Http\Request();
        $agent = Agents::findFirst('(email="' .  $request->getPost('email').'" OR username="' .$request->getPost('email').'") AND  password="'. sha1($request->getPost('password')).'" AND status=1');
        if($agent) {
            $jwt = new \Security\Jwt\JWT();
            $payload = array(
                "id" => $agent->id,
                "username" => $agent->username,
                "lastname" => $agent->last_name,
                "firstname" => $agent->first_name,
                "profile_pic_name" => $agent->profile_pic_name,
                "level" => $app->getConfig()['clients']['agent'],
                "exp" => time() + (60 * 60)
            ); // time in the future

            $token = $jwt->encode($payload, $app->getConfig()['hashkey']);
            $app->storeRedis($agent->id,$token);
            echo json_encode(array('data' => $token));
        }else{
            echo json_encode(array('error' => 'Username or Password is invalid.'));
        }
    }

    public function logoutAction(){
        $request = new \Phalcon\Http\Request();
        $jwt = new \Security\Jwt\JWT();
        $parsetoken = explode(" ",$request->getHeader('Authorization'));
        $app = new CB();
        $token = $jwt->decode($parsetoken[1], $app->getConfig()['hashkey'], array('HS256'));

        if($app->removeRedis($token->id)){
            die(json_encode(array("success" => 'Succcess')));
        }else{
            die(json_encode(array("error" => 'Succcess')));
        }

    }
    public function skipAction($name) {
        echo "auth skipped ($name)";
    }

    public function getpinAgentAction($id) {
        $agent = Agents::findFirst('id="' . $id . '"');
        if($agent){
            echo json_encode($agent->toArray());
        }else {
            echo "asdasd";
        }
    }

    public function getInfo($id) {
        $agent = Agents::findFirst('id="' . $id . '"');
        if($agent){
            echo json_encode($agent);
        }
    }

    public function updateprofileAction() {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            $usave = Agents::findFirst("id='" . $request->getPost('id') . "'");
            if($usave){
                $usave->first_name = $request->getPost('first_name');
                $usave->last_name = $request->getPost('last_name');
                $usave->gender = $request->getPost('gender');
                $usave->country = $request->getPost('country');
                $usave->state = $request->getPost('state');
                $usave->birthday = $request->getPost('year') . '-' . $request->getPost('month') . '-' . $request->getPost('day');
                $usave->updated_at = date("Y-m-d H:i:s");
                $usave->profile_pic_name = $request->getPost('profile_pic_name');
                $usave->mood = nl2br($request->getPost('mood'));
                if($usave->save()) {
                    $data = array('success' => 'Your profile has been successfully updated.' );
                }else {
                    $data = array('error' => 'An error occured, please try again later.' );
                }
            }else {
                $data = array('error' => 'An error occured, please try again later.' );
            }
            echo json_encode($data);
        }
    }

    public function notificationAction($id) {
        $pendingpin = 0;
        $app = new CB();
        $sql = "SELECT id FROM maps WHERE agent = '".$id."'";
        $maps = $app->dbSelect($sql);
        if($maps){
            foreach($maps as $key=>$value){
              $sqlCount = "SELECT COUNT(*)  FROM markers WHERE markers.map_id = '".$maps[$key]['id']."' AND status=0";
              $pendingpin = $pendingpin + $app->dbSelect($sqlCount)[0]["COUNT(*)"];
            }
        }

        $total = $pendingpin;
        echo json_encode(array('pendingpin' => $pendingpin, 'total' => $total));
    }

    public function checkpasswordAction($id) {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            $pass = sha1($request->getPost('password'));
            $agent = Agents::findFirst("id='$id' AND password='$pass'");
            if($agent){
                $data = array('success' => 'Correct password' );
            }else {
                $data = array('error' => 'Incorrect password' );
            }
            echo json_encode($data);
        }
    }

    public function updateUsernameEmailACtion($id) {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            $input = $request->getPost('input');
            $type = $request->getPost('type');
            $chck = Agents::findFirst("username='" . $uname . "' AND id!='$id'");
            if($chck){
                $data = array('error' => 'Username already exist');
            }else {
                $agent = Agents::findFirst("id='$id'");
                if($agent){
                    if($type == 'username'){
                        $agent->username = $input;
                    }else {
                        $agent->email = $input;
                    }
                    if($agent->save()){
                        $data = array('success' => 'Account has been updated successfully' );
                    }else {
                        $data = array('error' => 'An error occured, please try again later.' );
                    }
                }else {
                    $data = array('error' => 'An error occured, please try again later.' );
                }
            }

            echo json_encode($data);
        }
    }

    public function changepassAction($id) {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            $password = sha1($request->getPost('password'));
            $a = Agents::findFirst("id='$id'");
            if($a){
                $a->password = $password;
                if($a->save()){
                    $data = array('success' => 'Password has been changed successfully' );
                }else {
                    $data = array('error' => 'An error occured, please try again later.' );
                }
            }else {
                $data = array('error' => 'An error occured, please try again later.' );
            }
            echo json_encode($data);
        }
    }

    public function loadgalleryAction($id) {
        $img = Agentimage::find("agent='$id'");
        if($img){
            echo json_encode($img->toArray());
        }else {
            echo json_encode(array('error' => 'An error occurred please try again later.' ));
        }
    }

    public function saveprofilepicAction() {
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            $pp = new Agentimage();
            $guid = new \Utilities\Guid\Guid();
            $pp->assign(array(
                'id' =>  $guid->GUID(),
                'agent' => $request->getPost('id'),
                'filename' => $request->getPost('filename')));
            if($pp->save()){
                $data = array('success' => 'success' );
            }else {
                $data = array('error' => 'An error occurred, please try again later.' );
            }
            echo json_encode($data);
        }
    }

    public function deletepicAction($agentid)
    {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            $filename = $request->getPost('filename');
            $ai = Agentimage::findFirst("agent='$agentid' AND filename='$filename'");
            if($ai){
                if($ai->delete()){
                    $data = array('success' => 'success' );
                }else {
                    $data = array('error' => 'An error occurred please try again later.');
                }
                echo json_encode($data);
            }
        }
    }
}
