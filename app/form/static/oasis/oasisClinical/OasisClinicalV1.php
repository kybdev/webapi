<?php
namespace Form\Stat;

use Form\Template\OasisTemplate;

class OasisClinicalV1{

    public static function cons(){

        /**
         *  Structure
         *  => [index title]
         *      => Row Column
         *         1 = single row
         *         2 = Multiple
         *      => Field Template
         *      => Table Structure
         */

        return array(
            "M0010" => array(
                "row" => 1,
                "tamplate" => OasisTemplate::inputToLastMo(array(
                    "mo"    => "(M0010)", 
                    "titleB"=> "CMS Certification Number",
                    "model" => "M0010"
                )),
                "table" => array( "type"=> "char", "length"=> 45, "null" => true)
            ),

            "M0014" => array(
                "tamplate" => OasisTemplate::inputToLastMo(array(
                    "mo"    => "(M0014)",
                    "titleB"=> "Branch State",
                    "model" => "M0014"
                )),
                "table" => array( "type"=> "char", "length"=> 45, "null"=> true
                )
            ),

            "M0016" => array(
                "tamplate" => OasisTemplate::inputToLastMo(array(
                    "mo"    => "(M0016)",
                    "titleB"=> "Branch ID Number",
                    "model" => "M0016"
                )),
                "table" => array("type"=> "char", "length"=> 45, "null"=> true)
            ),

            "M0018" => array(
                "tamplate" => OasisTemplate::moTxtLabel(array(
                    "mo"    => "(M0018)",
                    "titleB"=> "National Provider Identifier (NPI)",
                    "titleN"=> "for the attending physician who has signed the plan of care:",
                )),
            ),



        );

    }
}
