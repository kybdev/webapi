<?php
/**
 * @author GEEKSNEST fork from Jete O'Keeffe
 * @version 1.0
 * @link http://docs.phalconphp.com/en/latest/reference/micro.html#defining-routes
 * @eg.
 */
return [
    "prefix" => "/dashboard",
    "handler" => 'Controllers\DashboardController',
    "lazy" => true,
    "collection" => [
        [
            'method' => 'get',
            'route' => '/record/{memberid}/{page}/{count}/{keyword}',
            'function' => 'index',
            'authentication' => true,
            'resource' => ['pm-apg','pm-apr']
        ],
        [
            'method' => 'get',
            'route' => '/fetchadmincounter',
            'function' => 'fetchadmincounter',
            'authentication' => true,
            'resource' => ['pm-apg','pm-apr']
        ],
        [
            'method' => 'get',
            'route' => '/fetchcliniciancounter',
            'function' => 'fetchcliniciancounter',
            'authentication' => true,
            'resource' => ['pm-apg','pm-apr']
        ],
        [
            'method' => 'get',
            'route' => '/fetchqacmcounter',
            'function' => 'fetchqacmcounter',
            'authentication' => true,
            'resource' => ['pm-apg','pm-apr']
        ],
        [
            'method' => 'post',
            'route' => '/fetchtasks',
            'function' => 'fetchtasks',
            'authentication' => true,
            'resource' => ['pm-apg','pm-apr']
        ],
        [
            'method' => 'post',
            'route' => '/qacm/fetchpatient',
            'function' => 'fetchpatient',
            'authentication' => true,
            'resource' => ['pm-ae','pc-cli', 'pc-cm', 'pc-cr','pc-hha','pc-msw', 'pc-qam', 'pc-rps','pc-cal','pc-att','pc-aps','pc-admin','pc-aam']
        ],
        [
            'method' => 'get',
            'route' => '/fetchtasksfilter',
            'function' => 'fetchtasksfilter',
            'authentication' => true,
            'resource' => ['pm-apg','pm-apr']
        ]
    ]
];
