<?php
/**
 * @author GEEKSNEST
 * @version 1.0
 * @link http://docs.phalconphp.com/en/latest/reference/micro.html#defining-routes
 * @eg.

return [
    "prefix" => "/v1/example",
    "handler" => 'Controllers\ExampleController',
    "lazy" => true,
    "collection" => [
        [
            'method' => 'get',
            'route' => '/ping',
            'function' => 'pingAction',
            'authentication' => FALSE,
            'resource' => 'rl1' // OPTIONAL if Authentication is True
        ]
    ]
];

 */
return [
    "prefix" => "/v1/system/management",
    "handler" => 'Controllers\SystemController',
    "lazy" => true,
    "collection" => [
        [
            'method' => 'get',
            'route' => '/recalibrate_roles',
            'function' => 'initializeRoles',
            'authentication' => false
        ],
        [
            'method' => 'get',
            'route' => '/recalibrate_user_role/{memberid}',
            'function' => 'generateUserCacheAcl',
            'authentication' => false
        ],
        [
            'method' => 'get',
            'route' => '/navigation',
            'function' => 'navigation',
            'authentication' => true,
            'resource' => 'sys-000'
        ],
        [
            'method' => 'get',
            'route' => '/systemtest1',
            'function' => 'test1',
            'authentication' => true,
            'resource' => ['ads-set', 'ads-age']
        ],
        [
            'method' => 'get',
            'route' => '/systemtest2',
            'function' => 'test2',
            'authentication' => true,
            'resource' => ['ads-sl', 'ads-plg']
        ],
        [
            'method' => 'get',
            'route' => '/systemtest3',
            'function' => 'test3',
            'authentication' => true,
            'resource' => ['ads-set','ads-plg','ads-sl']
        ],
        [
            'method' => 'get',
            'route' => '/systemtest4',
            'function' => 'test4',
            'authentication' => true,
            'resource' => ['ads-set','ads-plg','ads-sl','ads-age' ]
        ],
        [
            'method' => 'get',
            'route' => '/systemtest5',
            'function' => 'testpmap',
            'authentication' => true,
            'resource' => 'pm-ap'
        ],
        [
            'method' => 'get',
            'route' => '/systemtest6',
            'function' => 'testnew',
            'authentication' => true,
            'resource' => 'pm-ap'
        ],
        [
            'method' => 'post',
            'route' => '/pending/admission/nonmedicare/v1',
            'function' => 'bagongroute',
            'authentication' => true,
            'resource' => 'pm-ap'
        ]
    ]
];
